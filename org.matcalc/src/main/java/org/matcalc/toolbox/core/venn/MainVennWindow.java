/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.venn;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.abh.common.Mathematics;
import org.abh.common.bioinformatics.ui.groups.Group;
import org.abh.common.bioinformatics.ui.groups.GroupsGuiFileFilter;
import org.abh.common.bioinformatics.ui.groups.GroupsModel;
import org.abh.common.bioinformatics.ui.groups.GroupsPanel;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.geom.IntPos2D;
import org.abh.common.io.FileUtils;
import org.abh.common.io.PathUtils;
import org.abh.common.text.Join;
import org.abh.common.ui.BorderService;
import org.abh.common.ui.ModernComponent;
import org.abh.common.ui.UI;
import org.abh.common.ui.UIService;
import org.abh.common.ui.button.ModernButtonWidget;
import org.abh.common.ui.clipboard.ClipboardRibbonSection;
import org.abh.common.ui.contentpane.CenterTab;
import org.abh.common.ui.contentpane.HTab;
import org.abh.common.ui.contentpane.ModernHContentPane;
import org.abh.common.ui.contentpane.SizableContentPane;
import org.abh.common.ui.dialog.DialogEvent;
import org.abh.common.ui.dialog.DialogEventListener;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.event.ModernSelectionListener;
import org.abh.common.ui.graphics.CanvasMouseEvent;
import org.abh.common.ui.graphics.ModernCanvasMouseAdapter;
import org.abh.common.ui.graphics.icons.QuickOpenVectorIcon;
import org.abh.common.ui.graphics.icons.QuickSaveVectorIcon;
import org.abh.common.ui.help.ModernAboutDialog;
import org.abh.common.ui.help.RibbonPanelProductInfo;
import org.abh.common.ui.io.FileDialog;
import org.abh.common.ui.io.JpgGuiFileFilter;
import org.abh.common.ui.io.OpenRibbonPanel;
import org.abh.common.ui.io.PdfGuiFileFilter;
import org.abh.common.ui.io.PngGuiFileFilter;
import org.abh.common.ui.io.RecentFilesService;
import org.abh.common.ui.io.SaveAsRibbonPanel;
import org.abh.common.ui.io.SvgGuiFileFilter;
import org.abh.common.ui.options.ModernOptionsRibbonPanel;
import org.abh.common.ui.panel.CardPanel;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.ribbon.QuickAccessButton;
import org.abh.common.ui.ribbon.RibbonMenuItem;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarLocation;
import org.abh.common.ui.scrollpane.ScrollBarPolicy;
import org.abh.common.ui.splitpane.ModernVSplitPaneLine;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernClipboardTextArea;
import org.abh.common.ui.text.ModernLabel;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.widget.tooltip.ModernToolTip;
import org.abh.common.ui.window.ModernRibbonWindow;
import org.abh.common.ui.zoom.ModernStatusZoomSlider;
import org.abh.common.ui.zoom.ZoomModel;
import org.abh.common.ui.zoom.ZoomRibbonSection;
import org.apache.batik.transcoder.TranscoderException;
import org.graphplot.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// TODO: Auto-generated Javadoc
/**
 * The Class MainVennWindow.
 */
public class MainVennWindow extends ModernRibbonWindow implements ModernClickListener, ModernSelectionListener  {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The m open panel. */
	private OpenRibbonPanel mOpenPanel = new OpenRibbonPanel();

	/** The m save as panel. */
	private SaveAsRibbonPanel mSaveAsPanel = new SaveAsRibbonPanel();

	//private ModernStatusBar mStatusBar = new ModernStatusBar();

	/** The Constant LOG. */
	private static final Logger LOG = 
			LoggerFactory.getLogger(MainVennWindow.class);

	/** The m content pane. */
	private ModernHContentPane mContentPane = 
			new ModernHContentPane();

	/** The m groups panel. */
	private GroupsPanel mGroupsPanel;

	/** The m groups model. */
	private GroupsModel mGroupsModel = new GroupsModel();
	
	/** The m venn canvas. */
	private ProportionalVennCanvas mVennCanvas = 
			new ProportionalVennCanvas();
	
	/** The m zoom model. */
	private ZoomModel mZoomModel = new ZoomModel();

	/** The m style model. */
	private StyleModel mStyleModel = new StyleModel();

	/** The m format pane. */
	private VBox mFormatPane;

	/** The m text values. */
	private ModernClipboardTextArea mTextValues = 
			new ModernClipboardTextArea();

	/** The m overlap label. */
	private ModernLabel mOverlapLabel = new ModernAutoSizeLabel();

	/**
	 * The Class ExportCallBack.
	 */
	private class ExportCallBack implements DialogEventListener {
		
		/** The m file. */
		private Path mFile;
		
		/** The m pwd. */
		private Path mPwd;

		/**
		 * Instantiates a new export call back.
		 *
		 * @param file the file
		 * @param pwd the pwd
		 */
		public ExportCallBack(Path file, 
				Path pwd) {
			mFile = file;
			mPwd = pwd;
		}

		/* (non-Javadoc)
		 * @see org.abh.common.ui.dialog.DialogEventListener#statusChanged(org.abh.common.ui.dialog.DialogEvent)
		 */
		@Override
		public void statusChanged(DialogEvent e) {
			if (e.getStatus() == ModernDialogStatus.OK) {
				try {
					save(mFile);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				try {
					export(mPwd);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * The Class GroupEvents.
	 */
	private class GroupEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.common.event.ChangeListener#changed(org.abh.common.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			mVennCanvas.setGroups(mGroupsModel, mStyleModel.get());
		}	
	}
	
	/**
	 * The Class MouseEvents.
	 */
	private class MouseEvents extends ModernCanvasMouseAdapter {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.graphics.ModernCanvasMouseListener#canvasMouseClicked(org.abh.common.ui.graphics.CanvasMouseEvent)
		 */
		@Override
		public void canvasMouseClicked(CanvasMouseEvent e) {
			IntPos2D p = e.getScaledPos();
			
			boolean in1 = mVennCanvas.getP1() != null ? Mathematics.getEuclidianD(mVennCanvas.getP1(), p) <= mVennCanvas.getR1() : false;
			boolean in2 = mVennCanvas.getP2() != null ? Mathematics.getEuclidianD(mVennCanvas.getP2(), p) <= mVennCanvas.getR2() : false;
			boolean in3 = mVennCanvas.getP3() != null ? Mathematics.getEuclidianD(mVennCanvas.getP3(), p) <= mVennCanvas.getR3() : false;
			
			//System.err.println(in1 + " " + in2 + " " + in3 + " " + mVennCanvas.getP1() + " " + p + " " + mVennCanvas.getR2());
			
			List<String> lines = new ArrayList<String>();
			
			if (in1 && in2 && in3) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI123()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(0).getName(),
						"and",
						mGroupsModel.get(1).getName(),
						"and",
						mGroupsModel.get(2).getName(),
						"only (" + lines.size() + "):").toString());
			} else if (in1 && in2) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI12()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(0).getName(),
						"and",
						mGroupsModel.get(1).getName(),
						"only (" + lines.size() + "):").toString());
				
			} else if (in1 && in3) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI13()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(0).getName(),
						"and",
						mGroupsModel.get(2).getName(),
						"only (" + lines.size() + "):").toString());
			} else if (in2 && in3) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI23()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(1).getName(),
						"and",
						mGroupsModel.get(2).getName(),
						"only (" + lines.size() + "):").toString());
			} else if (in1) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI1()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(0).getName(),
						"only (" + lines.size() + "):").toString());
			} else if (in2) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI2()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(1).getName(),
						"only (" + lines.size() + "):").toString());
			} else if (in3) {
				lines.addAll(CollectionUtils.toString(mVennCanvas.getI3()));
				
				mOverlapLabel.setText(Join.onSpace().values("Values in",
						mGroupsModel.get(2).getName(),
						"only (" + lines.size() + "):").toString());
			} else {
				// Do nothing
			}
			
			mTextValues.setText(lines);
		}
	}

	/**
	 * Instantiates a new main venn window.
	 */
	public MainVennWindow() {
		super(new VennInfo());

		setup();
	}

	/**
	 * Instantiates a new main venn window.
	 *
	 * @param group1 the group 1
	 * @param group2 the group 2
	 * @param style the style
	 */
	public MainVennWindow(Group group1, Group group2, CircleStyle style) {
		super(new VennInfo());

		setup();
		
		mGroupsPanel.addGroup(group1);
		mGroupsPanel.addGroup(group2);
		
		mStyleModel.set(style);
	}
	
	/**
	 * Instantiates a new main venn window.
	 *
	 * @param group1 the group 1
	 * @param group2 the group 2
	 * @param group3 the group 3
	 * @param style the style
	 */
	public MainVennWindow(Group group1, Group group2, Group group3, CircleStyle style) {
		super(new VennInfo());

		setup();
		
		mGroupsPanel.addGroup(group1);
		mGroupsPanel.addGroup(group2);
		mGroupsPanel.addGroup(group3);
		
		mStyleModel.set(style);
	}

	/**
	 * Setup.
	 */
	private void setup() {
		mGroupsPanel = new GroupsPanel(this, mGroupsModel);

		createRibbon();

		createUi();
		
		mGroupsModel.addChangeListener(new GroupEvents());
		
		mStyleModel.addChangeListener(new GroupEvents());

		setSize(1200, 900);

		UI.centerWindowToScreen(this);
	}

	/**
	 * Creates the ribbon.
	 */
	/* (non-Javadoc)
	 * @see org.abh.common.ui.window.ModernRibbonWindow#createRibbon()
	 */
	public final void createRibbon() {
		//RibbongetRibbonMenu() getRibbonMenu() = new RibbongetRibbonMenu()(0);
		RibbonMenuItem ribbonMenuItem;

		ribbonMenuItem = new RibbonMenuItem("Open");
		//menuItem.setToolTipText("Select an input file.");
		getRibbonMenu().addTabbedMenuItem(ribbonMenuItem, mOpenPanel);


		ribbonMenuItem = new RibbonMenuItem(UI.MENU_SAVE_AS);
		getRibbonMenu().addTabbedMenuItem(ribbonMenuItem, mSaveAsPanel);

		ribbonMenuItem = new RibbonMenuItem(UI.MENU_EXIT);
		getRibbonMenu().addTabbedMenuItem(ribbonMenuItem);

		getRibbonMenu().addSeparator();

		ribbonMenuItem = new RibbonMenuItem(UI.MENU_INFO);
		getRibbonMenu().addTabbedMenuItem(ribbonMenuItem, 
				new RibbonPanelProductInfo(getAppInfo()));

		ribbonMenuItem = new RibbonMenuItem(UI.MENU_SETTINGS);
		getRibbonMenu().addTabbedMenuItem(ribbonMenuItem, 
				new ModernOptionsRibbonPanel(getAppInfo()));

		getRibbonMenu().addClickListener(this);



		//Ribbon2 ribbon = new Ribbon2();
		getRibbon().setHelpButtonEnabled(getAppInfo());

		ModernButtonWidget button = 
				new QuickAccessButton(UIService.getInstance().loadIcon(QuickOpenVectorIcon.class, 16));
		button.setClickMessage("Open");
		button.setToolTip(new ModernToolTip("Open", 
				"Open a gene list."));
		button.addClickListener(this);
		addQuickAccessButton(button);

		button = new QuickAccessButton(UIService.getInstance().loadIcon(QuickSaveVectorIcon.class, 16));
		button.setClickMessage(UI.MENU_SAVE);
		button.setToolTip(new ModernToolTip("Save", 
				"Save the current gene list."));
		button.addClickListener(this);
		addQuickAccessButton(button);

		// home

		getRibbon().getHomeToolbar().addSection(new ClipboardRibbonSection(getRibbon()));

		getRibbon().getHomeToolbar().addSection(new StyleRibbonSection(getRibbon(), mStyleModel));
	
		/*
		toolbarSection = new RibbonSection("Data");

		RibbonHItemsContainer toolbarContainer = new RibbonHItemsContainer();

		ModernPopupMenu openProjectPopup = new RecentFilesPopup(RecentFiles.getInstance());

		ModernDropDownMenuButton openButton = new RibbonLargeOptionalDropDownMenuButton("Open",
							Resources.getInstance().loadIcon("open", Resources.ICON_SIZE_32), openProjectPopup);

		openButton.addClickListener(this);
		toolbarContainer.add(openButton);

		ModernButtonWidget button = new RibbonLargeButton("Export", Resources.getInstance().loadIcon("export", Resources.ICON_SIZE_32));
		button.addClickListener(this);
		toolbarContainer.add(button);

		toolbarSection.setContent(toolbarContainer);
		toolbar.add(toolbarSection);
		 */

		// pick a chip

		getRibbon().getToolbar("View").add(new ZoomRibbonSection(this, mZoomModel));

		/*
		toolbar = new RibbonToolbar("View");

		toolbarSection = new RibbonSection("Show");

		toolbarContainer = new RibbonHItemsContainer();

		toolbarSection.setContent(toolbarContainer);

		toolbar.add(toolbarSection);

		toolbar.add(new WindowRibbonSection(this, ribbon));

		getRibbon().addToolbar(toolbar);
		*/

		//tabbedBar.setCanvasSize(new Dimension(Short.MAX_VALUE, 130));

		//setRibbon(ribbon, getRibbonMenu());

		getRibbon().setSelectedIndex(1);
	}



	/* (non-Javadoc)
	 * @see org.abh.common.ui.window.ModernWindow#createUi()
	 */
	public final void createUi() {
		setBody(mContentPane);
		
		mStatusBar.addRight(new ModernStatusZoomSlider(mZoomModel));

		//ZoomCanvas zoomCanvas = new ZoomCanvas(mVennCanvas);
		
		mVennCanvas.addCanvasMouseListener(new MouseEvents());
		
		//ZoomCanvas zoomCanvas = new ZoomCanvas(canvas);
		
		mVennCanvas.setZoomModel(mZoomModel);
		
		//BackgroundCanvas backgroundCanvas = new BackgroundCanvas(zoomCanvas);
		
		ModernScrollPane scrollPane1 = new ModernScrollPane(mVennCanvas)
				.setScrollBarLocation(ScrollBarLocation.FLOATING);
		
		
		ModernComponent panel = new ModernComponent();
		
		panel.setHeader(mOverlapLabel);
		
		ModernScrollPane scrollPane2 = new ModernScrollPane(mTextValues)
				.setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER);
		panel.setBody(new ModernComponent(scrollPane1, BorderService.getInstance().createTopBorder(10)));
			
		ModernVSplitPaneLine splitPane = new ModernVSplitPaneLine();
		
		splitPane.addComponent(new ModernComponent(new CardPanel(new ModernPanel(panel, ModernWidget.BORDER)), ModernWidget.DOUBLE_BORDER), 0.7);
		splitPane.addComponent(new ModernComponent(new CardPanel(new ModernPanel(scrollPane2, ModernWidget.BORDER)), ModernWidget.DOUBLE_BORDER), 0.3);
		
		//splitPane.setDividerLocation(600);

		mContentPane.getModel().setCenterTab(new CenterTab(splitPane));

		addGroupsPanel();
		
		mFormatPane = VBox.create();
		
		mFormatPane.add(new RadiusControl(mVennCanvas.getProperties()));
		
		//addFormatPane();
	}

	/**
	 * Adds the groups panel.
	 */
	private void addGroupsPanel() {
		if (mContentPane.getModel().getLeftTabs().containsTab("Groups")) {
			return;
		}

		SizableContentPane sizePane = new SizableContentPane("Groups", 
				mGroupsPanel, 
				300, 
				300, 
				500);

		mContentPane.getModel().addLeftTab(sizePane);
	}
	
	/**
	 * Adds the history pane to the layout if it is not already showing.
	 */
	private void addFormatPane() {
		if (mContentPane.getModel().getRightTabs().containsTab("Format Plot")) {
			return;
		}

		mContentPane.getModel().getRightTabs().addTab(new SizableContentPane("Format Plot", 
				new HTab("Format Plot", mFormatPane), 
				300, 
				200, 
				500));
		
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.event.ModernClickListener#clicked(org.abh.common.ui.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		if (e.getMessage().equals(UI.MENU_OPEN) ||
				e.getMessage().equals(UI.MENU_BROWSE) ||
				e.getMessage().startsWith("Other...")) {
			try {
				browseForFile();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(OpenRibbonPanel.FILE_SELECTED)) {
			try {
				openFile(mOpenPanel.getSelectedFile());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(OpenRibbonPanel.DIRECTORY_SELECTED)) {

			try {
				browseForFile(mOpenPanel.getSelectedDirectory());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(UI.MENU_SAVE)) {
			try {
				export();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (TranscoderException e1) {
				e1.printStackTrace();
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(SaveAsRibbonPanel.DIRECTORY_SELECTED)) {
			try {
				export(mSaveAsPanel.getSelectedDirectory());
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (TranscoderException e1) {
				e1.printStackTrace();
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(UI.MENU_ABOUT)) {
			ModernAboutDialog.show(this, getAppInfo());
		} else if (e.getMessage().equals(UI.MENU_EXIT)) {
			close();
		} else {
			// do nothing
		}
	}
	
	/**
	 * Browse for file.
	 *
	 * @throws Exception the exception
	 */
	private void browseForFile() throws Exception {
		browseForFile(RecentFilesService.getInstance().getPwd());
	}

	/**
	 * Browse for file.
	 *
	 * @param workingDirectory the working directory
	 * @throws Exception the exception
	 */
	private void browseForFile(Path workingDirectory) throws Exception {
		openFile(FileDialog.openFile(this, workingDirectory, new GroupsGuiFileFilter()));
	}

	/**
	 * Open file.
	 *
	 * @param file the file
	 * @throws Exception the exception
	 */
	private void openFile(Path file) throws Exception {
		if (file == null) {
			return;
		}

		LOG.info("Open file {}...", file);

		if (!FileUtils.exists(file)) {
			ModernMessageDialog.createFileDoesNotExistDialog(this, 
					getAppInfo().getName(), 
					file);

			return;
		}

		if (PathUtils.getFileExt(file).equals("mgrpx")) {
			List<Group> groups = Group.loadGroups(file);

			mGroupsPanel.addGroups(groups);
		} else {
			
		}

		RecentFilesService.getInstance().add(file);
	}

	/**
	 * Export.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void export() throws IOException, TranscoderException, TransformerException, ParserConfigurationException {
		export(RecentFilesService.getInstance().getPwd());
	}
	
	/**
	 * Export.
	 *
	 * @param pwd the pwd
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void export(Path pwd) throws IOException, TranscoderException, TransformerException, ParserConfigurationException {
		Path file = FileDialog.saveFile(this, 
				pwd, 
				new SvgGuiFileFilter(),
				new PngGuiFileFilter(),
				new PdfGuiFileFilter(),
				new JpgGuiFileFilter(),
				new GroupsGuiFileFilter());

		if (file == null) {
			return;
		}
		
		if (FileUtils.exists(file)) {
			ModernMessageDialog.createFileReplaceDialog(this, 
					file, 
					new ExportCallBack(file, pwd));
		} else {
			save(file);
		}
	}

	/**
	 * Save.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void save(Path file) throws IOException, TranscoderException, TransformerException, ParserConfigurationException {
		if (file == null) {
			return;
		}
		
		if (PathUtils.getFileExt(file).equals("mgrpx")) {
			mGroupsPanel.saveGroupsFile(file);
		} else {
			Image.write(mVennCanvas, file);
		}
		
		RecentFilesService.getInstance().setPwd(file.getParent());
		
		ModernMessageDialog.createFileSavedDialog(this, file);
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.event.ModernSelectionListener#selectionChanged(org.abh.common.event.ChangeEvent)
	 */
	@Override
	public void selectionChanged(ChangeEvent e) {
		
	}
}
