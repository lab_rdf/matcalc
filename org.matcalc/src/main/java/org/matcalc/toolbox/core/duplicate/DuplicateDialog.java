/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.duplicate;

import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.UI;
import org.abh.common.ui.dialog.ModernDialogHelpWindow;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.window.ModernWindow;

// TODO: Auto-generated Javadoc
/**
 * The class DuplicateDialog.
 */
public class DuplicateDialog extends ModernDialogHelpWindow implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	

	/**
	 * The member duplicate panel.
	 */
	private DuplicatePanel mDuplicatePanel;

	/**
	 * The member matrix.
	 */
	private AnnotationMatrix mMatrix;


	/**
	 * Instantiates a new duplicate dialog.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 */
	public DuplicateDialog(ModernWindow parent, 
			AnnotationMatrix matrix) {
		super(parent, "matcalc.modules.duplicate.help.url");
		
		setTitle("Duplicate Rows");
		
		mMatrix = matrix;
		
		setup();

		createUi();

	}

	/**
	 * Setup.
	 */
	private void setup() {
		setSize(320, 160);
		
		UI.centerWindowToScreen(this);
	}
	
	

	/**
	 * Creates the ui.
	 */
	private final void createUi() {
		//this.getContentPane().add(new JLabel("Change " + getProductDetails().getProductName() + " settings", JLabel.LEFT), BorderLayout.PAGE_START);
		
		mDuplicatePanel = new DuplicatePanel(mMatrix);
		
		setDialogCardContent(mDuplicatePanel);
	}

	/**
	 * Gets the delimiter.
	 *
	 * @return the delimiter
	 */
	public String getDelimiter() {
		return mDuplicatePanel.getDelimiter();
	}
}
