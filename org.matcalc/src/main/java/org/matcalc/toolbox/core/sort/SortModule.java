/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.abh.common.Mathematics;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.collections.DefaultTreeMap;
import org.abh.common.collections.TreeSetCreator;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.UIService;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.abh.common.ui.widget.tooltip.ModernToolTip;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcModule;


// TODO: Auto-generated Javadoc
/**
 * Can compare a column of values to another list to see what is common and
 * record this in a new column next to the reference column. Useful for
 * doing overlaps and keeping data in a specific order in a table.
 *
 * @author Antony Holmes Holmes
 *
 */
public class SortModule extends CalcModule implements ModernClickListener  {

	/**
	 * The member match button.
	 */
	private RibbonLargeButton mSortButton = 
			new RibbonLargeButton("Sort", UIService.getInstance().loadIcon("sort", 24));

	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Sort";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		mSortButton.setToolTip(new ModernToolTip("Sort", 
				"Sort columns."), mWindow.getRibbon().getToolTipModel());

		window.getRibbon().getToolbar("Data").getSection("Sort").add(mSortButton);

		mSortButton.addClickListener(this);

	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		sort();
	}

	/**
	 * Match.
	 */
	private void sort() {
		AnnotationMatrix m = mWindow.getCurrentMatrix();
		
		List<Integer> columns = mWindow.getSelectedColumns();
		
		int c = -1;
		
		if (columns.size() > 0) {
			c = columns.get(0);
		}
		
		SortDialog dialog = new SortDialog(mWindow, m, c);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}
		

		List<List<Integer>> sortedIds =
				new ArrayList<List<Integer>>();
		
		// Seed the sorter with an ordered list of all the rows
		sortedIds.add(Mathematics.sequence(0, m.getRowCount() - 1));

		//List<ColumnSort> sorters = new ArrayList<ColumnSort>();
		
		//sorters.add(new ColumnSort(m, 1, false));
		//sorters.add(new ColumnSort(m, 2));
		
		List<ColumnSort> sorters = dialog.getSorters();
		
		for (ColumnSort sorter : sorters) {
			c = sorter.getColumn();
			
			List<List<Integer>> newSortedIds = new ArrayList<List<Integer>>(); 
			
			for (List<Integer> ids : sortedIds) {
				// Sort the lists by key
				
				Map<String, Set<Integer>> sortMap =
						DefaultTreeMap.create(new TreeSetCreator<Integer>());
				
				for (int id : ids) {
					sortMap.get(m.getText(id, c)).add(id);
				}
				
				List<String> sortedKeys = 
						CollectionUtils.sort(sortMap.keySet(), NaturalSorter.INSTANCE);
				
				if (!sorter.getSortAsc()) {
					sortedKeys = CollectionUtils.reverse(sortedKeys);
				}
				
				// Create new list of sorted lists
				
				for (String key : sortedKeys) {
					newSortedIds.add(CollectionUtils.sort(sortMap.get(key)));
				}
			}
			
			// Swap the new ids for the old so that now we sort sub lists
			// on each loop
			sortedIds = newSortedIds;
		}
		
		// Create a master list of rows in sorted order
		List<Integer> rows = new ArrayList<Integer>(m.getRowCount());
		
		for (List<Integer> ids : sortedIds) {
			for (int id : ids) {
				rows.add(id);
			}
		}
		
		AnnotationMatrix ret = 
				AnnotatableMatrix.createAnnotatableMatrix(m.getRowCount(), m.getColumnCount());
		
		AnnotationMatrix.copyColumnAnnotations(m, ret);
		
		AnnotationMatrix.copyRows(m, rows, ret);
		
		mWindow.addToHistory("Sort matrix", ret);
	}
}
