/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.filter.column;

import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.icons.FilterVectorIcon;
import org.abh.common.ui.graphics.icons.Raster24Icon;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.abh.common.ui.theme.ThemeService;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcModule;


// TODO: Auto-generated Javadoc
/**
 * Row name.
 *
 * @author Antony Holmes Holmes
 */
public class ColumnFilterModule extends CalcModule implements ModernClickListener {

	/**
	 * The button.
	 */
	private RibbonLargeButton mButton = new RibbonLargeButton(new Raster24Icon(new FilterVectorIcon(ThemeService.getInstance().colors().getHighlight(8),
			ThemeService.getInstance().colors().getHighlight(6))),
			"Filter Columns",
			"Filter columns.");
	
	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Filter Columns";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;
		
		mButton.addClickListener(this);
		
		mWindow.getRibbon().getToolbar("Data").getSection("Filter").add(mButton);
	}
	
	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		order();
	}
		
	/**
	 * Order.
	 */
	private void order() {
		AnnotationMatrix m = mWindow.getCurrentMatrix();
		
		ColumnFilterDialog dialog = new ColumnFilterDialog(mWindow, m);
		
		dialog.setVisible(true);
		
		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}
		
		AnnotationMatrix ret = 
				AnnotatableMatrix.copyInnerColumns(m, dialog.getColumns());
		
		mWindow.addToHistory("Filter columns", ret);
	}
}
