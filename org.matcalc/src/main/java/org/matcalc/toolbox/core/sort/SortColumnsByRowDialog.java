/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.sort;

import javax.swing.Box;

import org.abh.common.ui.UI;
import org.abh.common.ui.button.CheckBox;
import org.abh.common.ui.button.ModernCheckBox;
import org.abh.common.ui.dialog.ModernDialogHelpWindow;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.window.ModernWindow;


// TODO: Auto-generated Javadoc
/**
 * Allow sorting of columns or rows in a table.
 *
 * @author Antony Holmes Holmes
 */
public class SortColumnsByRowDialog extends ModernDialogHelpWindow implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The model.
	 */
	private CheckBox mCheckByGroups = 
			new ModernCheckBox("Sort within groups");
	
	/** The m check asc. */
	private CheckBox mCheckAsc = new ModernCheckBox("Ascending", true);

	
	/**
	 * Instantiates a new sort column groups by row dialog.
	 *
	 * @param parent the parent
	 */
	public SortColumnsByRowDialog(ModernWindow parent) {
		super(parent, "matcalc.modules.sort-col-by-row.help.url");

		setup();
	}
	
	/**
	 * Setup.
	 */
	private void setup() {
		setTitle("Sort Columns By Row");
		
		createUi();

		setSize(400, 200);
		
		UI.centerWindowToScreen(this);
	}

	/**
	 * Creates the ui.
	 */
	private void createUi() {
		Box box = VBox.create();
		
		box.add(mCheckAsc);
		box.add(UI.createVGap(5));
		box.add(mCheckByGroups);
		
		setDialogCardContent(box);
	}

	/**
	 * Gets the sort within groups.
	 *
	 * @return the sort within groups
	 */
	public boolean getSortWithinGroups() {
		return mCheckByGroups.isSelected();
	}
	
	/**
	 * Gets the sort asc.
	 *
	 * @return the sort asc
	 */
	public boolean getSortAsc() {
		return mCheckAsc.isSelected();
	}
}
