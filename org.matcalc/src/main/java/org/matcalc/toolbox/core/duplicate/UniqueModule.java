/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core.duplicate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.abh.common.collections.UniqueArrayList;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.text.TextUtils;
import org.abh.common.ui.UIService;
import org.abh.common.ui.dialog.MessageDialogType;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.abh.common.ui.widget.tooltip.ModernToolTip;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcModule;


// TODO: Auto-generated Javadoc
/**
 * Collapse a file based on a repeated column. The selected column will
 * be reduced to a list of unique values whilst all other columns will be
 * turned into semi-colon separated lists of values.
 *
 * @author Antony Holmes Holmes
 *
 */
public class UniqueModule extends CalcModule implements ModernClickListener  {

	/**
	 * The member match button.
	 */
	private RibbonLargeButton mCollapseButton = 
			new RibbonLargeButton("Unique", UIService.getInstance().loadIcon("collapse", 24));

	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Unique";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		mCollapseButton.setToolTip(new ModernToolTip("Unique", 
				"Collapse rows on a column."), mWindow.getRibbon().getToolTipModel());

		window.getRibbon().getToolbar("Transform").getSection("Duplicate").add(mCollapseButton);

		mCollapseButton.addClickListener(this);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		if (e.getSource().equals(mCollapseButton)) {
			collapse();
		}
	}

	/**
	 * Match.
	 */
	private void collapse() {
		List<Integer> columns = mWindow.getSelectedColumns();

		if (columns == null || columns.size() == 0) {
			ModernMessageDialog.createDialog(mWindow, 
					"You must select a column of to match on.", 
					MessageDialogType.WARNING);
		}

		AnnotationMatrix m = mWindow.getCurrentMatrix();

		int c = columns.get(0);

		Map<String, List<Integer>> rows = new HashMap<String, List<Integer>>();
		List<String> ids = new UniqueArrayList<String>();

		for (int i = 0; i < m.getRowCount(); ++i) {
			String id = m.getText(i, c);
			
			ids.add(id);
			
			if (!rows.containsKey(id)) {
				rows.put(id, new ArrayList<Integer>());
			}
			
			rows.get(id).add(i);
		}

		AnnotationMatrix ret = AnnotatableMatrix.createAnnotatableMatrix(ids.size(), m.getColumnCount());

		ret.setColumnNames(m.getColumnNames());

		// first copy the annotations

		for (String name : m.getRowAnnotationNames()) {
			for (int i = 0; i < ids.size(); ++i) {
				List<Integer> indices = rows.get(ids.get(i));
				
				List<String> items = new UniqueArrayList<String>(indices.size());
				
				for (int k : indices) {
					items.add(m.getRowAnnotationText(name, k));
				}
				
				String text = TextUtils.scJoin(items);
				
				ret.setRowAnnotation(name, i, text);
			}
		}
		
		int max = 0;
		
		for (int row = 0; row < ids.size(); ++row) {
			List<Integer> indices = rows.get(ids.get(row));
			
			for (int column = 0; column < m.getColumnCount(); ++column) {
				//StringBuilder buffer = new StringBuilder(m.getText(indices.get(0), column));
				
				List<String> items = new UniqueArrayList<String>(indices.size());
				
				for (int k : indices) {
					items.add(m.getText(k, column));
				}
				
				String text = TextUtils.scJoin(items);
				
				System.err.println(row + " " + column + " " + text);
				
				ret.set(row, column, text);
				
				max = Math.max(max, text.length());	
			}
		}
		
		// Set the collapse column values to their unique values
		
	
		for (int row = 0; row < ids.size(); ++row) {
			//System.err.println("c " + row + " " + c + " " + ids.get(row));
			
			ret.set(row, c, ids.get(row));
		}
		
		//System.err.println("size " + ret.getColumnCount() + " " + ret.getRowCount());

		//mWindow.addToHistory("Duplicate rows", m);
		mWindow.addToHistory("Duplicate rows", ret);
	}
}
