/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.matcalc.toolbox.core;

import javax.swing.Box;

import org.abh.common.Mathematics;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.matrix.MatrixOperations;
import org.abh.common.ui.UI;
import org.abh.common.ui.dialog.ModernDialogTaskType;
import org.abh.common.ui.dialog.ModernDialogTaskWindow;
import org.abh.common.ui.panel.HExpandBox;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.text.ModernClipboardNumericalTextField;
import org.abh.common.ui.text.ModernTextBorderPanel;
import org.abh.common.ui.widget.ModernWidget;
import org.matcalc.MainMatCalcWindow;

// TODO: Auto-generated Javadoc
/**
 * User can enter an integer option value.
 * 
 * @author Antony Holmes Holmes
 *
 */
public class SummaryDialog extends ModernDialogTaskWindow {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	private MainMatCalcWindow mWindow;
	
	
	/**
	 * Instantiates a new modern int input dialog.
	 *
	 * @param parent the parent
	 * @param checked the checked
	 */
	public SummaryDialog(MainMatCalcWindow parent) {
		super(parent, false, ModernDialogTaskType.CLOSE);
		
		setTitle("Summary");
		
		mWindow = parent;
		
		setup();

		createUi();
	}

	/**
	 * Setup.
	 */
	private void setup() {
		setSize(400, 250);
		
		UI.centerWindowToScreen(this);
	}
	
	

	/**
	 * Creates the ui.
	 */
	private final void createUi() {
		//this.getContentPane().add(new JLabel("Change " + getProductDetails().getProductName() + " settings", JLabel.LEFT), BorderLayout.PAGE_START);
		
		AnnotationMatrix m = mWindow.getCurrentMatrix();
		
		Box box = VBox.create();
		
		box.add(new HExpandBox("Min", new ModernTextBorderPanel(new ModernClipboardNumericalTextField(Mathematics.round(MatrixOperations.min(m), 4), false), 100)));
		box.add(UI.createVGap(ModernWidget.PADDING));
		box.add(new HExpandBox("Max", new ModernTextBorderPanel(new ModernClipboardNumericalTextField(Mathematics.round(MatrixOperations.max(m), 4), false), 100)));
		box.add(UI.createVGap(ModernWidget.PADDING));
		box.add(new HExpandBox("Mean", new ModernTextBorderPanel(new ModernClipboardNumericalTextField(Mathematics.round(MatrixOperations.mean(m), 4), false), 100)));
		box.add(UI.createVGap(ModernWidget.PADDING));
		box.add(new HExpandBox("Median", new ModernTextBorderPanel(new ModernClipboardNumericalTextField(Mathematics.round(MatrixOperations.median(m), 4), false), 100)));

		setDialogCardContent(box);
	}
}
