/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.abh.common.collections.CollectionUtils;
import org.abh.common.collections.DefaultTreeMap;
import org.abh.common.collections.TreeSetCreator;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.text.TextUtils;
import org.abh.common.ui.UIService;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcModule;


// TODO: Auto-generated Javadoc
/**
 * Group items by an index column and return the counts for each unique
 * index. For example if there are two columns: gender and name, if gender
 * is selected as the index, it will create two groups, male and female and
 * track now many names occur in each group.
 *
 * @author Antony Holmes Holmes
 *
 */
public class GroupModule extends CalcModule implements ModernClickListener  {

	/**
	 * The member match button.
	 */
	private RibbonLargeButton mMatchButton = 
			new RibbonLargeButton("Group", UIService.getInstance().loadIcon("group", 24));

	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Group";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		mMatchButton.setToolTip("Group", "Group data.");

		window.getRibbon().getToolbar("Data").getSection("Tools").add(mMatchButton);

		mMatchButton.addClickListener(this);

	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		if (e.getMessage().equals("Group")) {
			match();
		}
	}

	/**
	 * Match.
	 */
	private void match() {
		AnnotationMatrix m = mWindow.getCurrentMatrix();
		
		if (m == null) {
			return;
		}
		
		List<Integer> columns = mWindow.getSelectedColumns();
		
		if (columns.size() < 2) {
			ModernMessageDialog.createWarningDialog(mWindow, "You must select an index column and a values column.");
			
			return;
		}

		Map<String, Set<String>> idMap = 
				DefaultTreeMap.create(new TreeSetCreator<String>()); //new TreeMap<String, Set<String>>();
		
		for (int i = 0; i < m.getRowCount(); ++i) {
			String id = m.getText(i, columns.get(0));
			String item = m.getText(i, columns.get(1));
			
			idMap.get(id).add(item);
		}
		
		AnnotationMatrix ret = 
				AnnotatableMatrix.createAnnotatableMatrix(idMap.size(), 3);
		
		ret.setName("Group");
		
		ret.setColumnName(0, m.getColumnName(columns.get(0)));
		ret.setColumnName(1, "Count");
		ret.setColumnName(2, m.getColumnName(columns.get(1)));

		List<String> ids = CollectionUtils.toList(idMap.keySet());
		
		for (int i = 0; i < ids.size(); ++i) {
			String id = ids.get(i);
			
			ret.set(i, 0, id);
			ret.set(i, 1, idMap.get(id).size());
			ret.set(i, 2, TextUtils.scJoin(CollectionUtils.toList(idMap.get(id))));
		}
		
		mWindow.openMatrix(ret);
	}
}
