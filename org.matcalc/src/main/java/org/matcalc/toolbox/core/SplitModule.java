/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.core;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.abh.common.Function;
import org.abh.common.collections.ArrayListMultiMap;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.collections.ListMultiMap;
import org.abh.common.io.FileUtils;
import org.abh.common.io.Io;
import org.abh.common.io.Temp;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.matrix.MatrixGroup;
import org.abh.common.stream.Stream;
import org.abh.common.ui.UIService;
import org.abh.common.ui.button.ModernDropDownButton;
import org.abh.common.ui.dialog.MessageDialogType;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.icons.ModernIcon;
import org.abh.common.ui.help.ModernMenuHelpItem;
import org.abh.common.ui.io.FileDialog;
import org.abh.common.ui.io.RecentFilesService;
import org.abh.common.ui.menu.ModernPopupMenu;
import org.abh.common.ui.menu.ModernTwoLineMenuItem;
import org.abh.common.ui.ribbon.RibbonLargeDropDownButton;
import org.graphplot.figure.series.XYSeries;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.OpenMode;
import org.matcalc.toolbox.CalcModule;


// TODO: Auto-generated Javadoc
/**
 * Split table into multiple tables by grouping column values and then create
 * a zip of the results.
 *
 * @author Antony Holmes Holmes
 *
 */
public class SplitModule extends CalcModule implements ModernClickListener  {

	/** The Constant ICON. */
	private static final ModernIcon ICON =
			UIService.getInstance().loadIcon("split", 24);

	/** The m button. */
	private ModernDropDownButton mButton;

	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Split";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		ModernPopupMenu popup = new ModernPopupMenu();

		popup.addMenuItem(new ModernTwoLineMenuItem("Split on column values", 
				"Split matrix by grouping rows in a column.", 
				ICON));
		popup.addMenuItem(new ModernTwoLineMenuItem("Split by group",
				"Split matrix based on column groups.",
				ICON));

		//popup.addMenuItem(new ModernMenuSeparator());
		
		popup.addMenuItem(new ModernMenuHelpItem("Help with splitting a matrix...",
				"matcalc.split.help.url").setTextOffset(48));

		mButton = new RibbonLargeDropDownButton("Split", ICON, popup);
		mButton.setChangeText(false);
		mButton.setToolTip("Split", "Split matrix into sub-matrices.");

		mWindow.getRibbon().getToolbar("Data").getSection("Tools").add(mButton);

		mButton.addClickListener(this);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		if (e.getMessage().equals("Split on column values")) {
			try {
				splitByColumn();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals("Split by group")) {
			try {
				splitByGroup();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} else {
			// Do nothing
		}
	}

	/**
	 * Split a matrix by grouping rows with the same value in a given column.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void splitByColumn() throws IOException {
		List<Integer> columns = mWindow.getSelectedColumns();

		if (columns == null || columns.size() == 0) {
			ModernMessageDialog.createDialog(mWindow, 
					"You must select a column to split on.", 
					MessageDialogType.WARNING);

			return;
		}

		SplitDialog dialog = new SplitDialog(mWindow);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		boolean openMatrices = dialog.getLoad();
		boolean createZip = dialog.getCreateZip();


		AnnotationMatrix current = mWindow.getCurrentMatrix();

		ListMultiMap<String, Integer> idMap = ArrayListMultiMap.create();

		int c = columns.get(0);

		// first the list of ids

		for (int i = 0; i < current.getRowCount(); ++i) {
			idMap.get(current.getText(i, c)).add(i);
		}

		// Clean up the ids
		List<String> ids = CollectionUtils.sort(idMap.keySet());


		List<String> cleanedIds = Stream.stream(ids)
				.map(new Function<String, String> () {
					@Override
					public String apply(String item) {
						return item
								.toLowerCase()
								.replaceAll("\\.[a-z]+$", "")
								.replaceAll("[^a-z0-9\\_\\-]", "_")
								.replaceAll("_+", "_");
					}})
				.toList();


		List<AnnotationMatrix> matrices = 
				new ArrayList<AnnotationMatrix>(ids.size());

		for (String id : ids) {
			System.err.println("split id " + id);
			matrices.add(AnnotatableMatrix.createAnnotatableMatrixFromRows(current, idMap.get(id)));
		}

		splitByGroup(cleanedIds, matrices, openMatrices, createZip);
	}

	/**
	 * Split by group.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void splitByGroup() throws IOException {
		List<XYSeries> groups = mWindow.getGroups();

		if (groups.size() == 0) {
			MainMatCalcWindow.createGroupWarningDialog(mWindow);

			return;
		}

		SplitDialog dialog = new SplitDialog(mWindow);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		boolean openMatrices = dialog.getLoad();
		boolean createZip = dialog.getCreateZip();

		AnnotationMatrix current = mWindow.getCurrentMatrix();

		List<List<Integer>> allIndices = 
				MatrixGroup.findColumnIndices(current, groups);

		List<AnnotationMatrix> matrices = 
				new ArrayList<AnnotationMatrix>(groups.size());

		for (List<Integer> indices : allIndices) {
			matrices.add(AnnotatableMatrix.createAnnotatableMatrixFromColumns(current, indices));
		}

		List<String> ids = new ArrayList<String>(groups.size());

		for (XYSeries group : groups) {
			ids.add(group
					.getName()
					.toLowerCase()
					.replaceAll("\\.[a-z]+$", "")
					.replaceAll("[^a-z0-9\\_\\-]", "_")
					.replaceAll("_+", "_"));
		}

		splitByGroup(ids, matrices, openMatrices, createZip);
	}

	/**
	 * Split by group.
	 *
	 * @param ids the ids
	 * @param matrices the matrices
	 * @param openMatrices the open matrices
	 * @param createZip the create zip
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void splitByGroup(List<String> ids,
			List<AnnotationMatrix> matrices,
			boolean openMatrices,
			boolean createZip) throws IOException {

		if (createZip) {
			Path file = FileDialog.saveZipFile(mWindow, 
					RecentFilesService.getInstance().getPwd());



			if (file != null) {
				boolean write = true;

				if (FileUtils.exists(file)) {
					ModernDialogStatus status = ModernMessageDialog.createFileReplaceDialog(mWindow, file);

					write = status == ModernDialogStatus.OK;
				}

				if (write) {
					List<Path> files = new ArrayList<Path>(ids.size());

					for (int i = 0; i < ids.size(); ++i) {
						String id = ids.get(i);

						AnnotationMatrix m = matrices.get(i);

						Path tmp = Temp.createTempFile(id + ".txt");

						AnnotationMatrix.writeAnnotationMatrix(m, tmp);

						files.add(tmp);
					}

					Io.createZip(file, files);

					ModernMessageDialog.createFileSavedDialog(mWindow, file);
				}
			}
		}

		if (openMatrices) {
			// We elect to open the matrices into matcalc
			mWindow.openMatrices(matrices, OpenMode.CURRENT_WINDOW);
		}
	}
}
