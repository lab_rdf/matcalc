/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.barchart;

import java.awt.Color;
import java.util.Set;

import org.abh.common.ColorUtils;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.matrix.MatrixGroup;
import org.abh.common.ui.UIService;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.graphplot.ColorCycle;
import org.graphplot.PlotFactory;
import org.graphplot.figure.Axes;
import org.graphplot.figure.Figure;
import org.graphplot.figure.SubFigure;
import org.graphplot.figure.series.XYSeries;
import org.graphplot.figure.series.XYSeriesGroup;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.figure.graph2d.Graph2dWindow;
import org.matcalc.toolbox.CalcModule;

// TODO: Auto-generated Javadoc
/**
 * The class BarChartModule.
 */
public class BarChartModule extends CalcModule implements ModernClickListener {
	
	/**
	 * The member parent.
	 */
	private MainMatCalcWindow mParent;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Bar Chart";
	}
	
	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mParent = window;
		
		RibbonLargeButton button = new RibbonLargeButton("Bar", "Chart", 
				UIService.getInstance().loadIcon("bar_chart", 24),
				"Bar Chart",
				"Generate a bar chart.");
		button.addClickListener(this);
		//button.setEnabled(false);

		mParent.getRibbon().getToolbar("Plot").getSection("Plot").add(button);
	}
	
	/**
	 * Creates the bar chart.
	 */
	private void createBarChart() {
		AnnotationMatrix m = mParent.getCurrentMatrix();
		
		XYSeriesGroup groups = mParent.getGroups();
		
		Figure figure = new Figure(); //window.getFigure();

		SubFigure subFigure = figure.getCurrentSubFigure();
		
		Axes axes = subFigure.getCurrentAxes();
		
		ColorCycle colorCycle = new ColorCycle();
		
		XYSeriesGroup seriesGroup = new XYSeriesGroup();

		for (int i = 0; i < m.getColumnCount(); ++i) {
			Color color = null;
			
			// See if the column matches a group, and if so use the group's
			// color rather than a random color
			for (MatrixGroup group : groups) {
				Set<Integer> indices = CollectionUtils.unique(MatrixGroup.findColumnIndices(m, group));
				
				if (indices.contains(i)) {
					color = group.getColor();
					
					break;
				}
			}
			
			if (color == null) {
				color = colorCycle.next();
			}
			
			XYSeries series = new XYSeries(m.getColumnName(i), color);
			
			series.getStyle().getFillStyle().setColor(ColorUtils.getTransparentColor50(color));
			series.getStyle().getLineStyle().setColor(color);

			seriesGroup.add(series);
		}
			
		/*
		for (MatrixGroup group : groups) {
			Color color = colorCycle.next();
			
			XYSeries series = new XYSeries(group.getName(), color);

			series.getStyle().getFillStyle().setColor(color);

			seriesGroup.add(series);
		}
		*/

		PlotFactory.createBarPlot(m, axes, seriesGroup);

		Graph2dWindow window = new Graph2dWindow(mParent, figure);
		
		window.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		createBarChart();
	}
}
