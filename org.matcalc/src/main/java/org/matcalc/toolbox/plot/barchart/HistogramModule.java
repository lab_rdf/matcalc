/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.barchart;

import java.awt.Color;

import org.abh.common.ColorUtils;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.statistics.HistBin;
import org.abh.common.math.statistics.Statistics;
import org.abh.common.ui.UIService;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.graphplot.ColorCycle;
import org.graphplot.PlotFactory;
import org.graphplot.figure.Axes;
import org.graphplot.figure.Figure;
import org.graphplot.figure.SubFigure;
import org.graphplot.figure.series.XYSeries;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.figure.graph2d.Graph2dWindow;
import org.matcalc.toolbox.CalcModule;

// TODO: Auto-generated Javadoc
/**
 * The class HistogramModule.
 */
public class HistogramModule extends CalcModule implements ModernClickListener {

	/**
	 * The member parent.
	 */
	private MainMatCalcWindow mParent;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Histogram";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mParent = window;

		RibbonLargeButton button = new RibbonLargeButton("Histogram", 
				UIService.getInstance().loadIcon("histogram", 24),
				"Histogram",
				"Generate a histogram.");
		button.addClickListener(this);
		//button.setEnabled(false);

		mParent.getRibbon().getToolbar("Plot").getSection("Plot").add(button);
	}

	/**
	 * Creates the plot.
	 */
	private void createPlot() {
		AnnotationMatrix m = mParent.getCurrentMatrix();

		Figure figure = new Figure();

		SubFigure subFigure = figure.getCurrentSubFigure();
		
		Axes axes = subFigure.getCurrentAxes();

		HistBin[] hist = Statistics.histogram(m.columnAsDouble(0), 1);

		ColorCycle colorCycle = new ColorCycle();
		
		Color color = colorCycle.next();
		
		XYSeries series = new XYSeries("Histogram", color);
		
		series.getStyle().getFillStyle().setColor(ColorUtils.getTransparentColor50(color));
		series.getStyle().getLineStyle().setColor(color);
		
		PlotFactory.createHistogram(m, axes, series, hist);

		Graph2dWindow window = new Graph2dWindow(mParent, figure);

		window.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		createPlot();
	}
}
