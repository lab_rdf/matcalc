/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.heatmap.cluster.legacy;

import java.io.IOException;
import java.util.List;

import org.abh.common.Properties;
import org.abh.common.math.cluster.Cluster;
import org.abh.common.math.cluster.DistanceMetric;
import org.abh.common.math.cluster.HierarchicalClustering;
import org.abh.common.math.cluster.Linkage;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.icons.Raster32Icon;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.graphplot.figure.heatmap.legacy.CountGroups;
import org.graphplot.figure.series.XYSeriesModel;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.icons.Cluster32VectorIcon;
import org.matcalc.toolbox.CalcModule;
import org.matcalc.toolbox.plot.heatmap.ClusterProperties;
import org.matcalc.toolbox.plot.heatmap.cluster.HierarchicalClusteringDialog;


// TODO: Auto-generated Javadoc
/**
 * Legacy cluster module using older heatmap module.
 *
 * @author Antony Holmes Holmes
 */
public class LegacyClusterModule extends CalcModule implements ModernClickListener  {

	/**
	 * The member match button.
	 */
	private RibbonLargeButton mClusterButton = new RibbonLargeButton("Cluster", 
			new Raster32Icon(new Cluster32VectorIcon()),
			"Cluster",
			"Cluster rows and columns.");

	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Cluster";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		window.getRibbon().getToolbar("Plot").getSection("Plot").add(mClusterButton);

		mClusterButton.addClickListener(this);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		try {
			cluster();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Cluster.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void cluster() throws IOException {
		cluster(new ClusterProperties());
	}

	/**
	 * Cluster.
	 *
	 * @param properties the properties
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void cluster(Properties properties) throws IOException {
		AnnotationMatrix m = mWindow.getCurrentMatrix();

		if (m == null) {
			return;
		}

		if (mWindow.getHistoryPanel().getItemCount() == 0) {
			return;
		}

		HierarchicalClusteringDialog dialog = 
				new HierarchicalClusteringDialog(mWindow);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		DistanceMetric distanceMetric = dialog.getDistanceMetric();

		Linkage linkage = dialog.getLinkage();

		cluster(m,
				distanceMetric,
				linkage, 
				dialog.clusterRows(),
				dialog.clusterColumns(), 
				dialog.optimalLeafOrder(),
				properties);
	}

	/**
	 * Cluster.
	 *
	 * @param m the m
	 * @param distanceMetric the distance metric
	 * @param linkage the linkage
	 * @param clusterRows the cluster rows
	 * @param clusterColumns the cluster columns
	 * @param optimalLeafOrder the optimal leaf order
	 * @param properties the properties
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void cluster(AnnotationMatrix m,
			DistanceMetric distanceMetric,
			Linkage linkage,
			boolean clusterRows,
			boolean clusterColumns,
			boolean optimalLeafOrder,
			Properties properties) throws IOException {
		
		cluster(mWindow,
				m,
				distanceMetric,
				linkage,
				clusterRows,
				clusterColumns,
				optimalLeafOrder,
				properties);
		
		/*
		if (m == null) {
			return;
		}

		Matrix im = m.getInnerMatrix();

		Cluster rowCluster = null;
		Cluster columnCluster = null;


		if (clusterRows) {
			rowCluster = HierarchicalClustering.rowCluster(im, 
					linkage,
					distanceMetric,
					optimalLeafOrder);
		}

		if (clusterColumns) {
			columnCluster = HierarchicalClustering.columnCluster(im,
					linkage,
					distanceMetric,
					optimalLeafOrder);
		}

		if (rowCluster == null && columnCluster == null) {
			return;
		}

		List<Integer> rowOrder;
		List<Integer> columnOrder;

		if (rowCluster != null) {
			rowOrder = Cluster.getLeafOrderedIds(rowCluster);
		} else {
			rowOrder = Mathematics.sequence(0, im.getRowCount() - 1);
		}

		if (columnCluster != null) {
			columnOrder = Cluster.getLeafOrderedIds(columnCluster);
		} else {
			columnOrder = Mathematics.sequence(0, im.getColumnCount() - 1);
		}

		AnnotationMatrix m2 = 
				AnnotatableMatrix.copyInnerRows(m, rowOrder);

		AnnotationMatrix m3 = 
				AnnotatableMatrix.copyInnerColumns(m2, columnOrder);

		mWindow.addToHistory("Cluster ordered matrix", m3);

		// So we can plot each row using a color
		//RowStandardizeMatrixView normalizedMatrix = new RowStandardizeMatrixView(m);


		//System.err.println("cluster " + clusters.size());

		//previewPanel.addPreview(new PreviewTablePanel("Collapsed " + fileCounter, false, collapsedFile));

		System.err.println("Cluster " + clusterRows + " " + clusterColumns);

		List<String> history = mWindow.getTransformationHistory();

		mWindow.addToHistory(new ClusterPlotMatrixTransform(mWindow, 
				m3, 
				XYSeriesModel.create(mWindow.getGroups()), 
				XYSeriesModel.create(mWindow.getRowGroups()),
				rowCluster, 
				columnCluster,
				CountGroups.defaultGroup(m),
				history,
				properties));
				*/
	}
	
	/**
	 * Cluster.
	 *
	 * @param window the window
	 * @param m the m
	 * @param distanceMetric the distance metric
	 * @param linkage the linkage
	 * @param clusterRows the cluster rows
	 * @param clusterColumns the cluster columns
	 * @param optimalLeafOrder the optimal leaf order
	 * @param properties the properties
	 * @return the annotation matrix
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static AnnotationMatrix cluster(MainMatCalcWindow window,
			AnnotationMatrix m,
			DistanceMetric distanceMetric,
			Linkage linkage,
			boolean clusterRows,
			boolean clusterColumns,
			boolean optimalLeafOrder,
			Properties properties) throws IOException {

		if (m == null) {
			return null;
		}

		Cluster rowCluster = null;
		Cluster columnCluster = null;


		if (clusterRows) {
			rowCluster = HierarchicalClustering.rowCluster(m, 
					linkage,
					distanceMetric,
					optimalLeafOrder);
		}

		if (clusterColumns) {
			columnCluster = HierarchicalClustering.columnCluster(m,
					linkage,
					distanceMetric,
					optimalLeafOrder);
		}

		if (rowCluster == null && columnCluster == null) {
			return m;
		}

		AnnotationMatrix m2;
		
		if (rowCluster != null) {
			List<Integer> rowOrder = Cluster.getLeafOrderedIds(rowCluster);
			
			m2 = AnnotatableMatrix.copyRows(m, rowOrder);
		} else {
			//rowOrder = Mathematics.sequence(0, m.getRowCount() - 1);
			m2 = m;
		}

		AnnotationMatrix m3;
		
		if (columnCluster != null) {
			List<Integer> columnOrder = Cluster.getLeafOrderedIds(columnCluster);
			
			m3 = AnnotatableMatrix.copyInnerColumns(m2, columnOrder);
		} else {
			//columnOrder = Mathematics.sequence(0, m.getColumnCount() - 1);
			m3 = m2;
		}

		window.addToHistory("Cluster ordered matrix", m3);

		// So we can plot each row using a color
		//RowStandardizeMatrixView normalizedMatrix = new RowStandardizeMatrixView(m);


		//System.err.println("cluster " + clusters.size());

		//previewPanel.addPreview(new PreviewTablePanel("Collapsed " + fileCounter, false, collapsedFile));

		/*
		ClusterPlotWindow window = new ClusterPlotWindow(m,
				groups,
				rowCluster,
				columnCluster);

		window.setVisible(true);
		 */

		System.err.println("Cluster " + clusterRows + " " + clusterColumns + " " + m3.getColumnCount());

		List<String> history = window.getTransformationHistory();
		
		//for (int i = 0; i < m3.getColumnCount(); ++i) {
		//	System.err.println("c " + i + " " + m3.getColumnName(i));
		//}

		window.addToHistory(new ClusterPlotMatrixTransform(window, 
				m3, 
				XYSeriesModel.create(window.getGroups()), 
				XYSeriesModel.create(window.getRowGroups()),
				rowCluster, 
				columnCluster,
				CountGroups.defaultGroup(m3),
				history,
				properties));
		
		return m3;
	}
}
