/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.heatmap.cluster.legacy;

import java.awt.Graphics2D;
import java.util.List;

import org.abh.common.Properties;
import org.abh.common.geom.IntDim;
import org.abh.common.math.cluster.Cluster;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.settings.SettingsService;
import org.abh.common.ui.graphics.DrawingContext;
import org.abh.common.ui.graphics.colormap.ColorMap;
import org.graphplot.ColorBar;
import org.graphplot.ModernPlotCanvas;
import org.graphplot.PlotElement;
import org.graphplot.figure.heatmap.legacy.BottomColumnLabelPlotElement;
import org.graphplot.figure.heatmap.legacy.ColumnLabelPosition;
import org.graphplot.figure.heatmap.legacy.ColumnLabelProperties;
import org.graphplot.figure.heatmap.legacy.CountBracketLeftPlotElement;
import org.graphplot.figure.heatmap.legacy.CountBracketRightPlotElement;
import org.graphplot.figure.heatmap.legacy.CountGroups;
import org.graphplot.figure.heatmap.legacy.CountPlotElement;
import org.graphplot.figure.heatmap.legacy.GroupProperties;
import org.graphplot.figure.heatmap.legacy.GroupsLegendPlotElement;
import org.graphplot.figure.heatmap.legacy.HeatMapPlotElement;
import org.graphplot.figure.heatmap.legacy.MatrixSummaryPlotElement;
import org.graphplot.figure.heatmap.legacy.RowLabelPosition;
import org.graphplot.figure.heatmap.legacy.RowLabelProperties;
import org.graphplot.figure.heatmap.legacy.RowLabelsPlotElement;
import org.graphplot.figure.heatmap.legacy.TopColumnLabelPlotElement;
import org.graphplot.figure.heatmap.legacy.clustering.ColumnHTreeTopPlotElement;
import org.graphplot.figure.heatmap.legacy.clustering.GroupsHierarchicalPlotElement;
import org.graphplot.figure.heatmap.legacy.clustering.RowHTreeLeftPlotElement;
import org.graphplot.figure.heatmap.legacy.clustering.RowHTreeRightPlotElement;
import org.graphplot.figure.heatmap.legacy.expression.GroupColorBarPlotElement;
import org.graphplot.figure.series.XYSeriesGroup;
import org.graphplot.plotbox.PlotBox;
import org.graphplot.plotbox.PlotBoxCanvas;
import org.graphplot.plotbox.PlotBoxColumn;
import org.graphplot.plotbox.PlotBoxEmpty;
import org.graphplot.plotbox.PlotBoxRow;


// TODO: Auto-generated Javadoc
/**
 * The class ClusterCanvas.
 */
public class ClusterCanvas extends ModernPlotCanvas {

	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The constant BLOCK_SIZE.
	 */
	private static final int BLOCK_SIZE = 
			SettingsService.getInstance().getAsInt("plot.block-size");

	/**
	 * The horizontal gap.
	 */
	private static IntDim HOZ_GAP = new IntDim(BLOCK_SIZE, 0);

	/**
	 * The vertical gap.
	 */
	private static IntDim VERTICAL_GAP = new IntDim(0, BLOCK_SIZE);


	/**
	 * The constant CHAR_WIDTH.
	 */
	private static final int CHAR_WIDTH = 
			SettingsService.getInstance().getAsInt("graphplot.plot.char-width");

	/**
	 * The constant LEGEND_WIDTH.
	 */
	private static final int LEGEND_WIDTH = 
			SettingsService.getInstance().getAsInt("plot.legend-width");

	/**
	 * The constant COLOR_BAR_WIDTH.
	 */
	private static final int COLOR_BAR_WIDTH = 
			SettingsService.getInstance().getAsInt("graphplot.plot.color-bar-width");

	/** The Constant COLOR_BAR_HEIGHT. */
	private static final int COLOR_BAR_HEIGHT = 
			SettingsService.getInstance().getAsInt("graphplot.plot.color-bar-height");

	//private static final int treeWidth = 200;

	/**
	 * The member plot box.
	 */
	private PlotBox mPlotBox;

	/**
	 * Instantiates a new cluster canvas.
	 *
	 * @param matrix the matrix
	 * @param groups the groups
	 * @param rowGroups the row groups
	 * @param countGroups the count groups
	 * @param history the history
	 * @param min the min
	 * @param max the max
	 * @param rowLabelProperties the row label properties
	 * @param columnLabelProperties the column label properties
	 * @param groupProperties the group properties
	 * @param properties the properties
	 */
	public ClusterCanvas(AnnotationMatrix matrix,
			XYSeriesGroup groups,
			XYSeriesGroup rowGroups,
			CountGroups countGroups,
			List<String> history,
			double min,
			double max,
			RowLabelProperties rowLabelProperties,
			ColumnLabelProperties columnLabelProperties,
			GroupProperties groupProperties,
			Properties properties) {
		this(matrix, 
				null, 
				null, 
				groups, 
				rowGroups, 
				countGroups, 
				history, 
				min, 
				max, 
				rowLabelProperties, 
				columnLabelProperties, 
				groupProperties, 
				properties);
	}

	/**
	 * Instantiates a new cluster canvas.
	 *
	 * @param matrix the matrix
	 * @param rowCluster the row cluster
	 * @param columnCluster the column cluster
	 * @param groups the groups
	 * @param rowGroups the row groups
	 * @param countGroups the count groups
	 * @param history the history
	 * @param min the min
	 * @param max the max
	 * @param rowLabelProperties the row label properties
	 * @param columnLabelProperties the column label properties
	 * @param groupProperties the group properties
	 * @param properties the properties
	 */
	public ClusterCanvas(AnnotationMatrix matrix,
			Cluster rowCluster,
			Cluster columnCluster,
			XYSeriesGroup groups,
			XYSeriesGroup rowGroups,
			CountGroups countGroups,
			List<String> history,
			double min,
			double max,
			RowLabelProperties rowLabelProperties,
			ColumnLabelProperties columnLabelProperties,
			GroupProperties groupProperties,
			Properties properties) {
		
		PlotBox emptyVBox = new PlotBoxEmpty(VERTICAL_GAP);
		PlotBox emptyHBox = new PlotBoxEmpty(HOZ_GAP);

		mPlotBox = new PlotBoxRow();

		// Add space at the top
		mPlotBox.addChild(emptyVBox);


		PlotElement element;

		PlotBox columnBox;



		// row labels

		PlotBox rowLabelsBox = new PlotBoxColumn();

		IntDim aspectRatio = (IntDim)properties.getProperty("plot.block-size");

		ColorMap colorMap = 
				(ColorMap)properties.getProperty("plot.colormap");

		int maxChars = properties.getAsInt("plot.row-label-max-chars");

		int treeWidthH = properties.getAsInt("plot.tree.hoz.width");
		boolean treeVisH = properties.getAsBool("plot.tree.hoz.visible");
		int treeWidthV = properties.getAsInt("plot.tree.vert.width");
		boolean treeVisV = properties.getAsBool("plot.tree.vert.visible");

		if (rowLabelProperties.show) {
			if (rowLabelProperties.showFeatures &&
					rowLabelProperties.position == RowLabelPosition.RIGHT && 
					matrix.getRowCount() > 1) {
				rowLabelsBox.addChild(emptyHBox);

				element = new CountBracketRightPlotElement(matrix, 
						countGroups, 
						10, 
						aspectRatio,
						rowLabelProperties.color);
				
				rowLabelsBox.addChild(new PlotBoxCanvas(element));

				rowLabelsBox.addChild(emptyHBox);

				element = new CountPlotElement(matrix,
						countGroups,
						100, 
						aspectRatio, 
						rowLabelProperties.color);

				rowLabelsBox.addChild(new PlotBoxCanvas(element));
			}


			/*
			if (rowCluster != null) {
				element = new RowHierarchicalLabelPlotElement(matrix, 
						rowCluster, 
						rowLabelProperties,
						aspectRatio, 
						CHAR_WIDTH);
			 */
			//} else {
			element = new RowLabelsPlotElement(matrix,
					rowLabelProperties,
					aspectRatio,
					CHAR_WIDTH);
			//}

			rowLabelsBox.addChild(new PlotBoxCanvas(element));

			if (rowLabelProperties.showFeatures &&
					rowLabelProperties.position == RowLabelPosition.LEFT && 
					matrix.getRowCount() > 1) {
				rowLabelsBox.addChild(emptyHBox);

				element = new CountPlotElement(matrix, 
						countGroups,
						100, 
						aspectRatio, 
						rowLabelProperties.color);
				
				rowLabelsBox.addChild(new PlotBoxCanvas(element));

				rowLabelsBox.addChild(emptyHBox);

				element = new CountBracketLeftPlotElement(matrix, 
						10, 
						aspectRatio,
						rowLabelProperties.color);
				rowLabelsBox.addChild(new PlotBoxCanvas(element));
			}
		}
	
		PlotBox rowLabelsSpaceBox = 
				new PlotBoxEmpty(rowLabelsBox.getCanvasSize().getW(), 0);

		if (columnLabelProperties.position == ColumnLabelPosition.TOP) {
			element = new TopColumnLabelPlotElement(matrix,
					groups,
					aspectRatio,
					properties,
					columnLabelProperties.color,
					CHAR_WIDTH, 
					maxChars);
		} else {
			element = new BottomColumnLabelPlotElement(matrix, 
					aspectRatio,
					columnLabelProperties.color, 
					CHAR_WIDTH, 
					maxChars);
		}

		PlotBox columnLabelsBox = new PlotBoxCanvas(element);

		// Now we build the plot


		if (columnCluster != null) {
			columnBox = new PlotBoxColumn();
			columnBox.addChild(emptyHBox);

			if (treeVisH && rowCluster != null) {
				columnBox.addChild(new PlotBoxEmpty(treeWidthH, -1));
				columnBox.addChild(emptyHBox);
			}

			if (rowLabelProperties.show && 
					rowLabelProperties.position == RowLabelPosition.LEFT) {
				columnBox.addChild(rowLabelsSpaceBox);
				columnBox.addChild(emptyHBox);
			}

			if (treeVisV && columnCluster != null) {
				element = new ColumnHTreeTopPlotElement(matrix,
						groups,
						aspectRatio,
						treeWidthV, 
						columnCluster,
						properties);
				//canvas.setCanvasSize(new Dimension(-1, columnClusterHeight));
				columnBox.addChild(new PlotBoxCanvas(element));
			}

			mPlotBox.addChild(columnBox);

			mPlotBox.addChild(emptyVBox);
		}


		//
		// Color bar
		//

		if (groupProperties.showColors && groups.getCount() > 0) {
			columnBox = new PlotBoxColumn();

			columnBox.addChild(emptyHBox);

			if (rowLabelProperties.show && 
					rowLabelProperties.position == RowLabelPosition.LEFT) {
				columnBox.addChild(rowLabelsSpaceBox);
				columnBox.addChild(emptyHBox);
			}

			if (treeVisH && rowCluster != null) {
				columnBox.addChild(new PlotBoxEmpty(treeWidthH, -1));
				columnBox.addChild(emptyHBox);
			}

			/*
			if (columnCluster != null) {
				element = new GroupHierarchicalColorBarPlotElement(matrix,
						aspectRatio,
						BLOCK_SIZE, 
						columnCluster, 
						groups,
						groupProperties);
			} else {
			 */
			element = new GroupColorBarPlotElement(matrix, 
					aspectRatio, 
					groups, 
					groupProperties);
			//}

			columnBox.addChild(new PlotBoxCanvas(element));
			mPlotBox.addChild(columnBox);

			mPlotBox.addChild(emptyVBox);
		}


		//
		// Column labels
		//

		if (columnLabelProperties.show && 
				columnLabelProperties.position == ColumnLabelPosition.TOP) {
			columnBox = new PlotBoxColumn();
			columnBox.addChild(emptyHBox);

			if (rowLabelProperties.show) {
				if (rowLabelProperties.position == RowLabelPosition.LEFT) {
					columnBox.addChild(rowLabelsSpaceBox);
					columnBox.addChild(emptyHBox);
				} else {
					if (treeVisH && rowCluster != null) {
						columnBox.addChild(new PlotBoxEmpty(treeWidthH, -1));
						columnBox.addChild(emptyHBox);
					}
				}
			}

			columnBox.addChild(columnLabelsBox);

			mPlotBox.addChild(columnBox);

			mPlotBox.addChild(emptyVBox);
		}

		//
		// cluster
		//

		boolean heatMapVisible = properties.getAsBool("plot.heatmap.visible");

		if (heatMapVisible) {
			columnBox = new PlotBoxColumn();

			columnBox.addChild(emptyHBox);

			if (rowLabelProperties.show) {
				if (rowLabelProperties.position == RowLabelPosition.LEFT) {
					columnBox.addChild(rowLabelsBox);
					columnBox.addChild(emptyHBox);
				} else {
					// If labels are on the right, then tree appears on the left

					if (treeVisH && rowCluster != null) {
						element = new RowHTreeLeftPlotElement(matrix, 
								treeWidthH, 
								aspectRatio, 
								rowCluster,
								properties.getAsColor("plot.tree.hoz.color"));
						//canvas.setCanvasSize(new Dimension(rowTreeProperties.width, -1));
						columnBox.addChild(new PlotBoxCanvas(element));

						columnBox.addChild(emptyHBox);
					}
				}
			}

			element = new HeatMapPlotElement(matrix,
					colorMap, 
					aspectRatio);
			((HeatMapPlotElement)element).setGridColor(properties.getAsColor("plot.grid-color"));
			((HeatMapPlotElement)element).setOutlineColor(properties.getAsColor("plot.outline-color"));
			((HeatMapPlotElement)element).setBorderColor(properties.getAsColor("plot.border-color"));


			columnBox.addChild(new PlotBoxCanvas(element));

			if (rowLabelProperties.show) {
				if (rowLabelProperties.position == RowLabelPosition.RIGHT) {
					columnBox.addChild(emptyHBox);
					columnBox.addChild(rowLabelsBox);
				} else {
					// If labels are on the right, then tree appears on the left

					if (treeVisH && rowCluster != null) {
						columnBox.addChild(emptyHBox);

						element = new RowHTreeRightPlotElement(matrix, 
								treeWidthH, 
								aspectRatio, 
								rowCluster,
								properties.getAsColor("plot.tree.hoz.color"));
						//canvas.setCanvasSize(new Dimension(rowTreeProperties.width, -1));
						columnBox.addChild(new PlotBoxCanvas(element));

						//columnBox.addChild(emptyHBox);
					}
				}
			}

			mPlotBox.addChild(columnBox);

			//
			// Space
			//
			mPlotBox.addChild(emptyVBox);
		}

		//
		// labels
		//

		columnBox = new PlotBoxColumn();

		columnBox.addChild(emptyHBox);

		if (rowLabelProperties.show) {
			if (rowLabelProperties.position == RowLabelPosition.LEFT) {
				columnBox.addChild(rowLabelsSpaceBox);
				columnBox.addChild(emptyHBox);
			} else {
				if (treeVisH && rowCluster != null) {
					columnBox.addChild(new PlotBoxEmpty(treeWidthH, 0));
					columnBox.addChild(emptyHBox);
				}
			}
		}

		if (columnLabelProperties.show && 
				columnLabelProperties.position == ColumnLabelPosition.BOTTOM) {
			columnBox.addChild(columnLabelsBox);
			columnBox.addChild(emptyHBox);
		}

		PlotBox rowBox = new PlotBoxRow();

		if (properties.getAsBool("plot.show-legend")) {
			if (columnCluster != null) {
				element = new GroupsHierarchicalPlotElement(matrix, 
						aspectRatio, 
						LEGEND_WIDTH, 
						columnCluster, 
						groups);
			} else {
				// Conventional heat map
				element = new GroupsLegendPlotElement(matrix, 
						aspectRatio, 
						LEGEND_WIDTH, 
						groups);
			}

			rowBox.addChild(new PlotBoxCanvas(element));
			rowBox.addChild(emptyVBox);
			element = new ColorBar(colorMap, min, max);
			element.setCanvasSize(COLOR_BAR_WIDTH, COLOR_BAR_HEIGHT);
			rowBox.addChild(new PlotBoxCanvas(element));

			rowLabelsBox.addChild(emptyVBox);
		}

		element = new MatrixSummaryPlotElement(matrix, 
				history, 
				aspectRatio, 
				400);
		rowBox.addChild(new PlotBoxCanvas(element));

		columnBox.addChild(rowBox);

		mPlotBox.addChild(columnBox);
		
		setPreferredSize(IntDim.toDimension(mPlotBox.getCanvasSize()));
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.plot.ModernPlotCanvas#plot(java.awt.Graphics2D, org.abh.lib.ui.modern.graphics.DrawingContext)
	 */
	@Override
	public void plot(Graphics2D g2, DrawingContext context) {
		mPlotBox.plot(g2, context);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.graphics.ModernCanvas#getCanvasSize()
	 */
	//@Override
	//public void setPreferredSize(Dimension d) {
	//	setPreferredSize(IntDim.toDimension(mPlotBox.getCanvasSize()));
	//}
	
	//public void setPreferredSize(IntDim d) {
	//	setPreferredSize(IntDim.toDimension(d));
	//}
}
