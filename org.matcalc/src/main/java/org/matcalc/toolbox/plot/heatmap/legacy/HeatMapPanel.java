/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.heatmap.legacy;

import java.awt.Color;
import java.util.List;

import javax.swing.Box;

import org.abh.common.Properties;
import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.geom.IntDim;
import org.abh.common.math.cluster.Cluster;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.matrix.MatrixOperations;
import org.abh.common.ui.UI;
import org.abh.common.ui.button.ModernCheckSwitch;
import org.abh.common.ui.collapsepane.AbstractCollapsePane;
import org.abh.common.ui.collapsepane.ModernSubCollapsePane;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.colormap.ColorMap;
import org.abh.common.ui.graphics.colormap.ColorMapModel;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarLocation;
import org.abh.common.ui.tabs.TabsModel;
import org.abh.common.ui.widget.ModernTwoStateWidget;
import org.abh.common.ui.window.ModernWindow;
import org.abh.common.ui.zoom.ZoomModel;
import org.graphplot.ModernPlotCanvas;
import org.graphplot.figure.heatmap.ColorNormalizationModel;
import org.graphplot.figure.heatmap.ColorNormalizationType;
import org.graphplot.figure.heatmap.legacy.ColumnLabelProperties;
import org.graphplot.figure.heatmap.legacy.CountGroups;
import org.graphplot.figure.heatmap.legacy.RowLabelPosition;
import org.graphplot.figure.heatmap.legacy.RowLabelProperties;
import org.graphplot.figure.series.XYSeries;
import org.graphplot.figure.series.XYSeriesGroup;
import org.graphplot.figure.series.XYSeriesModel;
import org.matcalc.figure.BlockSizeControl;
import org.matcalc.figure.CanvasPanel;
import org.matcalc.figure.CheckControl;
import org.matcalc.figure.ColoredPlotControl;
import org.matcalc.figure.ColumnLabelPositionPlotElement;
import org.matcalc.figure.FormatPlotPane;
import org.matcalc.figure.GroupsPlotControl;
import org.matcalc.figure.MatrixGroupControl;
import org.matcalc.figure.PlotConstants;
import org.matcalc.figure.RowLabelControl;
import org.matcalc.figure.ScaleControl;
import org.matcalc.figure.TreeControl;
import org.matcalc.toolbox.plot.heatmap.ScaleModel;
import org.matcalc.toolbox.plot.heatmap.cluster.legacy.ClusterCanvas;


// TODO: Auto-generated Javadoc
/**
 * The class HeatMapPanel.
 */
public class HeatMapPanel extends FormatPlotPane implements CanvasPanel, ModernClickListener, ChangeListener {

	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * The member canvas.
	 */
	protected ModernPlotCanvas mCanvas;

	/**
	 * The member matrix.
	 */
	protected AnnotationMatrix mMatrix;

	/**
	 * The member zoom model.
	 */
	private ZoomModel mZoomModel;

	//private StandardizationChooser standardizationChooser = 
	//		new StandardizationChooser();

	/**
	 * The member grid element.
	 */
	protected ColoredPlotControl mGridElement;

	/**
	 * The member outline element.
	 */
	private ColoredPlotControl mOutlineElement;

	/**
	 * The member border element.
	 */
	protected ColoredPlotControl mBorderElement;

	/**
	 * The member rows element.
	 */
	protected RowLabelControl mRowsElement;

	/**
	 * The member columns element.
	 */
	protected ColoredPlotControl mColumnsElement;

	/**
	 * The member check show legend.
	 */
	protected ModernTwoStateWidget mCheckShowLegend = 
			new ModernCheckSwitch("Legend", true);
	
	/** The m check show. */
	private ModernTwoStateWidget mCheckShow;

	/**
	 * The member aspect ratio element.
	 */
	protected BlockSizeControl mAspectRatioElement;

	/**
	 * The member column label position element.
	 */
	protected ColumnLabelPositionPlotElement mColumnLabelPositionElement;

	/**
	 * The member intensity element.
	 */
	protected ScaleControl mScaleElement;

	/**
	 * The member groups element.
	 */
	protected GroupsPlotControl mGroupsElement;

	/**
	 * The member history.
	 */
	protected List<String> mHistory;

	/**
	 * The member color standardization model.
	 */
	protected ColorNormalizationModel mColorStandardizationModel;


	/**
	 * The member properties.
	 */
	protected Properties mProperties;


	/**
	 * The member color map model.
	 */
	protected ColorMapModel mColorMapModel;


	/**
	 * The member content.
	 */
	private TabsModel mContent;

	/** The m scale model. */
	protected ScaleModel mScaleModel;

	/** The m groups model. */
	protected XYSeriesModel mGroupsModel;

	/** The m group display control. */
	private MatrixGroupControl mGroupDisplayControl;

	/** The m count groups. */
	protected CountGroups mCountGroups;

	/** The m row groups model. */
	protected XYSeriesModel mRowGroupsModel;

	/** The m row cluster. */
	protected Cluster mRowCluster;

	/** The m column cluster. */
	protected Cluster mColumnCluster;
	

	/**
	 * Instantiates a new heat map panel.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param groupsModel the groups model
	 * @param rowGroupsModel the row groups model
	 * @param countGroups the count groups
	 * @param history the history
	 * @param zoomModel the zoom model
	 * @param colorMapModel the color map model
	 * @param colorStandardizationModel the color standardization model
	 * @param scaleModel the scale model
	 * @param contentModel the content model
	 * @param properties the properties
	 */
	public HeatMapPanel(ModernWindow parent,
			AnnotationMatrix matrix,
			XYSeriesModel groupsModel,
			XYSeriesModel rowGroupsModel,
			CountGroups countGroups,
			List<String> history,
			ZoomModel zoomModel,
			ColorMapModel colorMapModel,
			ColorNormalizationModel colorStandardizationModel,
			ScaleModel scaleModel,
			TabsModel contentModel,
			Properties properties) {
		this(parent, 
				matrix, 
				null, 
				null, 
				groupsModel, 
				rowGroupsModel, 
				countGroups, 
				history, 
				zoomModel, 
				colorMapModel, 
				colorStandardizationModel, 
				scaleModel, 
				contentModel, 
				properties);
	}
	
	/**
	 * Instantiates a new heat map panel.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param rowCluster the row cluster
	 * @param columnCluster the column cluster
	 * @param groupsModel the groups model
	 * @param rowGroupsModel the row groups model
	 * @param countGroups the count groups
	 * @param history the history
	 * @param zoomModel the zoom model
	 * @param colorMapModel the color map model
	 * @param colorStandardizationModel the color standardization model
	 * @param scaleModel the scale model
	 * @param contentModel the content model
	 * @param properties the properties
	 */
	public HeatMapPanel(ModernWindow parent,
			AnnotationMatrix matrix,
			Cluster rowCluster,
			Cluster columnCluster,
			XYSeriesModel groupsModel,
			XYSeriesModel rowGroupsModel,
			CountGroups countGroups,
			List<String> history,
			ZoomModel zoomModel,
			ColorMapModel colorMapModel,
			ColorNormalizationModel colorStandardizationModel,
			ScaleModel scaleModel,
			TabsModel contentModel,
			Properties properties) {
		mContent = contentModel;

		mRowCluster = rowCluster;
		mColumnCluster = columnCluster;
		mCountGroups = countGroups;
		mMatrix = matrix;
		mGroupsModel = groupsModel;
		mRowGroupsModel = rowGroupsModel;
		mHistory = history;
		mZoomModel = zoomModel;
		mColorMapModel = colorMapModel;
		mColorStandardizationModel = colorStandardizationModel;
		mScaleModel = scaleModel;
		mProperties = properties;


		//
		// Heat map
		//

		AbstractCollapsePane rightPanel;

		Box box;
		
		rightPanel = new ModernSubCollapsePane();

		box = VBox.create();
		
		
		mCheckShow = new ModernCheckSwitch("Show", 
				properties.getAsBool("plot.heatmap.visible"));
		mCheckShow.addClickListener(this);
		mCheckShow.setAlignmentY(TOP_ALIGNMENT);
		box.add(mCheckShow);
		
		box.add(ModernPanel.createVGap());
		
		//Box box2 = VBox.create();
		//box2.setBorder(BorderService.getInstance().createLeftBorder(10));
		
		mGridElement = new ColoredPlotControl(parent, 
				"Grid",
				properties.getAsColor("plot.grid-color"),
				properties.getAsBool("plot.show-grid-color"));
		mGridElement.addClickListener(this);
		box.add(mGridElement);

		box.add(ModernPanel.createVGap());

		mOutlineElement = new ColoredPlotControl(parent, 
				"Outline",
				properties.getAsColor("plot.outline-color"),
				properties.getAsBool("plot.show-outline-color"));
		mOutlineElement.addClickListener(this);
		box.add(mOutlineElement);

		box.add(ModernPanel.createVGap());

		mBorderElement = new ColoredPlotControl(parent, 
				"Border",
				properties.getAsColor("plot.border-color"),
				properties.getAsBool("plot.border-color-enabled"));
		mBorderElement.addClickListener(this);
		box.add(mBorderElement);

		//box.setAlignmentY(TOP_ALIGNMENT);
		//box.add(box2);
		
		box.setBorder(LARGE_BORDER);
		
		//box.add(ModernPanel.createVGap());
		//rightPanel.addTab("Heat Map", box, true);

		
		rightPanel.addTab(PlotConstants.LABEL_HEATMAP, box, true);
		
		box = VBox.create();
		mAspectRatioElement = new BlockSizeControl((IntDim)properties.getProperty("plot.block-size"));
		mAspectRatioElement.addChangeListener(this);
		box.add(mAspectRatioElement);
		box.setBorder(LARGE_BORDER);
		rightPanel.addTab(PlotConstants.LABEL_BLOCK_SIZE, box, true);
		
		box = VBox.create();
		box.add(mCheckShowLegend);
		box.add(ModernPanel.createVGap());
		mScaleElement = new ScaleControl(scaleModel);
		box.add(mScaleElement);
		box.setBorder(LARGE_BORDER);
		rightPanel.addTab(PlotConstants.LABEL_LEGEND, box, true);
		
		
		mGroupTabsModel.addTab("HEAT MAP", rightPanel);

		// TEST
		//JScrollPane s = new JScrollPane(box);
		//s.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		//mGroupTabsModel.addTab("Rows", s);
		
		//
		// Rows
		//

		rightPanel = new ModernSubCollapsePane();

		// Only show this option if we have clusters to show
		if (mRowCluster != null) {
			box = VBox.create();
			box.add(new TreeControl(parent, properties, "plot.tree.hoz"));
			box.add(UI.createVGap(5));
			box.setBorder(LARGE_BORDER);
			rightPanel.addTab("Row Tree", box, true);
		}
		
		box = VBox.create();

		mRowsElement = new RowLabelControl(parent, 
				matrix,
				RowLabelPosition.RIGHT,
				properties.getAsBool("plot.show-row-labels"));

		box.add(mRowsElement);

		box.setBorder(LARGE_BORDER);

		rightPanel.addTab("Row Labels", box, true);

		
		mGroupTabsModel.addTab("ROWS", rightPanel); //new ModernScrollPane(rightPanel).setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER));

		//
		// Columns
		//

		rightPanel = new ModernSubCollapsePane();
		
		if (mColumnCluster != null) {
			box = VBox.create();
			box.add(new TreeControl(parent, properties, "plot.tree.vert"));
			box.add(UI.createVGap(5));
			
			box.setBorder(LARGE_BORDER);
			
			rightPanel.addTab("Tree", box, true);
		}

		box = VBox.create();

		mColumnsElement = new ColoredPlotControl(parent, 
				"Show",
				Color.BLACK);
		box.add(mColumnsElement);
		
		//box2 = VBox.create();
		
		box.add(new CheckControl(parent, 
				"Color by group", 
				properties, 
				"plot.labels.color-by-group"));

		//box.add(UI.createVGap(5));

		mColumnLabelPositionElement = 
				new ColumnLabelPositionPlotElement();

		box.add(mColumnLabelPositionElement);

		//box2.setBorder(BorderService.getInstance().createLeftBorder(10));
		
		//box.add(box2);
		
		box.setBorder(LARGE_BORDER);
		
		rightPanel.addTab("Column Labels", box, true);
		
		box = VBox.create();

		mGroupsElement = new GroupsPlotControl(parent, 
				Color.BLACK,
				properties);
		box.add(mGroupsElement);

		// Ability to switch on groups is only available to unclustered
		// data
		if (mColumnCluster == null && mGroupsModel.getTotalCount() > 0) {
			box.add(UI.createVGap(10));
			
			mGroupDisplayControl = 
					new MatrixGroupControl(mGroupsModel);
			
			box.add(mGroupDisplayControl);

			//rightPanel.addTab("Group Display", mGroupDisplayControl, true);
		}
		
		box.setBorder(LARGE_BORDER);
		
		rightPanel.addTab("Groups", box, true);

		mGroupTabsModel.addTab("COLUMNS", rightPanel); //new ModernScrollPane(rightPanel).setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER));

		mGroupsElement.addClickListener(this);
		mCheckShowLegend.addClickListener(this);
		//standardizationChooser.addClickListener(this);
		//mIntensityElement.addClickListener(this);
		mColumnLabelPositionElement.addClickListener(this);
		mRowsElement.addClickListener(this);
		mColumnsElement.addClickListener(this);

		mColorMapModel.addChangeListener(this);
		mColorStandardizationModel.addChangeListener(this);
		mScaleModel.addChangeListener(this);
		mProperties.addChangeListener(this);
		
		if (mGroupsModel != null) {
			mGroupsModel.addChangeListener(this);
		}

		mGroupTabsModel.changeTab(0);
	}

	/**
	 * Update.
	 */
	@Override
	public void update() {
		double max = mScaleModel.get(); //mIntensityModel.getBaseline();
		double min = -max; //mIntensityModel.getBaseline(); //PlotConstants.MIN_STD; // / scale;
		//PlotConstants.MAX_STD; // / scale;

		// Create version of the matrix with only the groups of interest
		XYSeriesGroup seriesOfInterest = new XYSeriesGroup();

		for (XYSeries series : mGroupsModel) {
			if (mGroupsModel.getVisible(series)) {
				seriesOfInterest.add(series);
			}
		}

		XYSeriesGroup rowSeriesOfInterest = new XYSeriesGroup();
		
		for (XYSeries series : mRowGroupsModel) {
			if (mRowGroupsModel.getVisible(series)) {
				rowSeriesOfInterest.add(series);
			}
		}

		AnnotationMatrix m = createMatrix(mMatrix, 
				seriesOfInterest, 
				rowSeriesOfInterest, 
				min, 
				max);

		display(m, seriesOfInterest, rowSeriesOfInterest, min, max);
	}
	
	/**
	 * Create a matrix (normalize for coloring etc) without filtering groups.
	 *
	 * @param m the m
	 * @param min the min
	 * @param max the max
	 * @return the annotation matrix
	 */
	public AnnotationMatrix createMatrix(AnnotationMatrix m,
			double min,
			double max) {
		
		return createMatrix(m, 
				XYSeriesGroup.EMPTY_GROUP, 
				XYSeriesGroup.EMPTY_GROUP, 
				min, 
				max);
	}

	/**
	 * Creates the matrix.
	 *
	 * @param m the m
	 * @param groupsOfInterest the groups of interest
	 * @param rowGroupsOfInterest the row groups of interest
	 * @param min the min
	 * @param max the max
	 * @return the annotation matrix
	 */
	public AnnotationMatrix createMatrix(AnnotationMatrix m,
			XYSeriesGroup groupsOfInterest,
			XYSeriesGroup rowGroupsOfInterest,
			double min,
			double max) {
		AnnotationMatrix ret = m;

		//double plotMin = Plot.MIN_STD;
		//double plotMax = Plot.MAX_STD;

		// For z-transforms default to being centered about 0
		//double scale = mIntensityModel.getScale(); //mIntensityModel.get(); //

		switch(mColorStandardizationModel.get().getType()) {
		case ZSCORE_MATRIX:
			ret = MatrixOperations.zscore(ret); // new MatrixZTransformView(mMatrix);
			break;
		case ZSCORE_ROW:
			ret = MatrixOperations.rowZscore(ret); //new RowZTransformMatrixView(mMatrix);
			break;
		case ZSCORE_COLUMN:
			ret = MatrixOperations.columnZscore(ret); //new ColumnZTransformMatrixView(mMatrix);
			break;
		case NORMALIZE:
			if (Double.isNaN(mColorStandardizationModel.get().getMax())) {
				// Auto use the min and max to normalize
				min = MatrixOperations.min(ret);
				max = MatrixOperations.max(ret);
			} else {
				// Use fixed bounds to normalize on.
				min = mColorStandardizationModel.get().getMin();
				max = mColorStandardizationModel.get().getMax();
			}

			break;
		default:
			// For the scale of no standardization, i.e we do not adjust
			// or normalize
			//min = MatrixOperations.min(ret);
			//max = MatrixOperations.max(ret);

			break;
		}
		
	
		if (groupsOfInterest.size() > 0) {
			ret = AnnotatableMatrix.copyInnerColumns(ret, groupsOfInterest);
		}

		if (mColorStandardizationModel.get().getType() != ColorNormalizationType.NONE) {
			//min /= scale;
			//max /= scale;

			ret = MatrixOperations.normalize(ret, min, max);
		}

		return ret;
	}

	/**
	 * Display.
	 *
	 * @param m the m
	 * @param groupsOfInterest the groups of interest
	 * @param rowGroupsOfInterest the row groups of interest
	 * @param min the min
	 * @param max the max
	 */
	public void display(AnnotationMatrix m,
			XYSeriesGroup groupsOfInterest,
			XYSeriesGroup rowGroupsOfInterest,
			double min,
			double max) {
		ColorMap colorMap = mColorMapModel.get();
		
		RowLabelProperties rowLabelProperties = new RowLabelProperties();
		
		rowLabelProperties.showFeatures = 
				mRowsElement.getShowFeatureCount();
		
		rowLabelProperties.show = 
				mRowsElement.isSelected();
		
		rowLabelProperties.color = 
				mRowsElement.getSelectedColor();
		
		rowLabelProperties.position = 
				mRowsElement.getPosition();

		mRowsElement.setShowAnnotations(rowLabelProperties.showAnnotations);

		ColumnLabelProperties columnLabelProperties = 
				new ColumnLabelProperties();
		
		columnLabelProperties.show = 
				mColumnsElement.isSelected();
		
		columnLabelProperties.color = 
				mColumnsElement.getSelectedColor();
		
		columnLabelProperties.position = 
				mColumnLabelPositionElement.getPosition();

		mProperties.updateProperty("plot.show-legend",
				mGroupsElement.getShowLegend());
		
		
		mProperties.updateProperty("plot.grid-color", 
				mGridElement.getSelectedColor());
		mProperties.updateProperty("plot.show-grid-color", 
				mGridElement.isSelected());

		mProperties.updateProperty("plot.outline-color", 
				mOutlineElement.getSelectedColor());
		mProperties.updateProperty("plot.show-outline-color", 
				mOutlineElement.isSelected());

		mProperties.updateProperty("plot.border-color", 
				mBorderElement.getSelectedColor());
		mProperties.updateProperty("plot.show-border-color", 
				mBorderElement.isSelected());

		//mProperties.updateProperty("plot.aspect-ratio", 
		//		mAspectRatioElement.getAspectRatio());
		
		mProperties.updateProperty("plot.block-size", 
				mAspectRatioElement.getBlockSize());

		mProperties.updateProperty("plot.colormap", colorMap);
		
		mProperties.updateProperty("plot.heatmap.visible", 
				mCheckShow.isSelected());
		
		mCanvas = createCanvas(m,
				groupsOfInterest,
				rowGroupsOfInterest,
				min,
				max,
				rowLabelProperties,
				columnLabelProperties);

		display(mCanvas);
	}

	/**
	 * Creates the canvas.
	 *
	 * @param m the m
	 * @param groupsOfInterest the groups of interest
	 * @param rowGroupsOfInterest the row groups of interest
	 * @param min the min
	 * @param max the max
	 * @param rowLabelProperties the row label properties
	 * @param columnLabelProperties the column label properties
	 * @return the modern plot canvas
	 */
	public ModernPlotCanvas createCanvas(AnnotationMatrix m,
			XYSeriesGroup groupsOfInterest,
			XYSeriesGroup rowGroupsOfInterest,
			double min,
			double max,
			RowLabelProperties rowLabelProperties,
			ColumnLabelProperties columnLabelProperties) {
		return new ClusterCanvas(m,
				mRowCluster,
				mColumnCluster,
				groupsOfInterest,
				rowGroupsOfInterest,
				mCountGroups,
				mHistory,
				min,
				max,
				rowLabelProperties,
				columnLabelProperties,
				mGroupsElement.getProperties(),
				mProperties);
	}

	/**
	 * Display.
	 *
	 * @param canvas the canvas
	 */
	public void display(ModernPlotCanvas canvas) {
		//ZoomCanvas zoomCanvas = new ZoomCanvas(canvas);

		canvas.setZoomModel(mZoomModel);

		//BackgroundCanvas backgroundCanvas = new BackgroundCanvas(zoomCanvas);

		ModernScrollPane scrollPane = new ModernScrollPane(canvas)
			.setVScrollBarLocation(ScrollBarLocation.FLOATING)
			.setHScrollBarLocation(ScrollBarLocation.FLOATING);

		//ModernComponent panel = new ModernComponent(scrollPane, BORDER);

		//panel.setBorder(ModernPanel.BORDER);

		mContent.setCenterTab(new ModernPanel(scrollPane, BORDER)); //new CenterTab(new ModernPanel(scrollPane, ModernWidget.BORDER)));
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.lib.bioinformatics.ui.plot.CanvasPanel#getCanvas()
	 */
	@Override
	public ModernPlotCanvas getCanvas() {
		return mCanvas;
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		update();
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
	 */
	@Override
	public void changed(ChangeEvent e) {
		update();
	}
}
