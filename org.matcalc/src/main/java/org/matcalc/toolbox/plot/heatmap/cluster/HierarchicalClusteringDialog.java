/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.plot.heatmap.cluster;

import javax.swing.Box;

import org.abh.common.math.cluster.AverageLinkage;
import org.abh.common.math.cluster.CompleteLinkage;
import org.abh.common.math.cluster.DistanceMetric;
import org.abh.common.math.cluster.Linkage;
import org.abh.common.math.cluster.SingleLinkage;
import org.abh.common.ui.UI;
import org.abh.common.ui.button.CheckBox;
import org.abh.common.ui.button.ModernCheckBox;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.dialog.ModernDialogHelpWindow;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.HExpandBox;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.window.ModernWindow;
import org.abh.common.ui.window.WindowWidgetFocusEvents;
import org.matcalc.figure.PlotConstants;


// TODO: Auto-generated Javadoc
/**
 * The class HierarchicalClusteringDialog.
 */
public class HierarchicalClusteringDialog extends ModernDialogHelpWindow implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The member distance combo.
	 */
	private ClusterDistanceMetricCombo mDistanceCombo = 
			new ClusterDistanceMetricCombo();
	
	/**
	 * The member linkage combo.
	 */
	private ModernComboBox mLinkageCombo =
			new ModernComboBox();
	
	/**
	 * The cluster columns check.
	 */
	private CheckBox clusterColumnsCheck = 
			new ModernCheckBox("Cluster columns", true);

	/**
	 * The cluster rows check.
	 */
	private CheckBox clusterRowsCheck = 
			new ModernCheckBox("Cluster rows");
	
	/** The m check optimal leaf order. */
	private CheckBox mCheckOptimalLeafOrder = 
			new ModernCheckBox("Optimal leaf order", true);

	/**
	 * The member check plot.
	 */
	private CheckBox mCheckPlot = 
			new ModernCheckBox(PlotConstants.MENU_SHOW_HEATMAP, true);

	
	/**
	 * Instantiates a new hierarchical clustering dialog.
	 *
	 * @param parent the parent
	 */
	public HierarchicalClusteringDialog(ModernWindow parent) {
		super(parent, "matcalc.cluster.help.url");
		
		setTitle("Cluster");
		
		setup();

		createUi();
	}

	/**
	 * Setup.
	 */
	private void setup() {
		addWindowListener(new WindowWidgetFocusEvents(mOkButton));
		
		mLinkageCombo.addMenuItem("Average");
		mLinkageCombo.addMenuItem("Complete");
		mLinkageCombo.addMenuItem("Single");
		mLinkageCombo.setSelectedIndex(0);
		
		mDistanceCombo.setSelectedIndex(3);

		setSize(400, 320);
		
		UI.centerWindowToScreen(this);
	}

	/**
	 * Creates the ui.
	 */
	private final void createUi() {
		Box box = Box.createVerticalBox();
		

		box.add(new HExpandBox("Distance", mDistanceCombo));
		box.add(ModernPanel.createVGap());
		box.add(new HExpandBox("Linkage", mLinkageCombo));
		box.add(ModernPanel.createVGap());
		box.add(clusterRowsCheck);
		box.add(ModernPanel.createVGap());
		box.add(clusterColumnsCheck);
		box.add(ModernPanel.createVGap());
		box.add(mCheckPlot);
		box.add(ModernPanel.createVGap());
		box.add(mCheckOptimalLeafOrder);
		
		//box.setBorder(ModernWidget.LARGE_BORDER);

		setDialogCardContent(box);
	}
	
	/**
	 * Gets the distance metric.
	 *
	 * @return the distance metric
	 */
	public DistanceMetric getDistanceMetric() {
		return mDistanceCombo.getDistanceMetric();
	}
	
	/**
	 * Gets the linkage.
	 *
	 * @return the linkage
	 */
	public Linkage getLinkage() {
		
		Linkage linkage;
		
		switch (mLinkageCombo.getSelectedIndex()) {
		case 1:
			linkage = new CompleteLinkage();
			break;
		case 2:
			linkage = new SingleLinkage();
			break;
		default:
			linkage = new AverageLinkage();
			break;
		}
		
		return linkage;
	}

	/**
	 * Cluster rows.
	 *
	 * @return true, if successful
	 */
	public boolean clusterRows() {
		return clusterRowsCheck.isSelected();
	}
	
	/**
	 * Cluster columns.
	 *
	 * @return true, if successful
	 */
	public boolean clusterColumns() {
		return clusterColumnsCheck.isSelected();
	}

	/**
	 * Gets the creates the plot.
	 *
	 * @return the creates the plot
	 */
	public boolean getCreatePlot() {
		return mCheckPlot.isSelected();
	}

	/**
	 * Optimal leaf order.
	 *
	 * @return true, if successful
	 */
	public boolean optimalLeafOrder() {
		return mCheckOptimalLeafOrder.isSelected();
	}
}
