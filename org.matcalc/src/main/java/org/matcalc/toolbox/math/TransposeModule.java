/**
normalize() * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.math;

import org.abh.common.math.matrix.MatrixOperations;
import org.abh.common.ui.UIService;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcWinModule;

// TODO: Auto-generated Javadoc
/**
 * The class ZScoreModule.
 */
public class TransposeModule extends CalcWinModule implements ModernClickListener {
	
	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Transpose";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		super.init(window);

		RibbonLargeButton button = new RibbonLargeButton("Transpose", 
				UIService.getInstance().loadIcon("transpose", 24),
				"Transpose", 
				"Transpose a matrix.");
		button.addClickListener(this);
		window.getRibbon().getToolbar("Transform").getSection("Transform").add(button);

	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		mWindow.addToHistory("Transpose", 
				"Transpose", 
				MatrixOperations.transpose(mWindow.getCurrentMatrix()));
	}
}
