/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.toolbox.math;

import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.math.matrix.MatrixOperations;
import org.abh.common.ui.UIService;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.icons.ModernIcon;
import org.abh.common.ui.help.ModernMenuHelpItem;
import org.abh.common.ui.menu.ModernPopupMenu;
import org.abh.common.ui.menu.ModernTwoLineMenuItem;
import org.abh.common.ui.ribbon.RibbonLargeOptionalDropDownButton;
import org.abh.common.ui.window.ModernWindow;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.toolbox.CalcModule;

// TODO: Auto-generated Javadoc
/**
 * The class LogModule.
 */
public class LogModule extends CalcModule implements ModernClickListener {
	
	/** The Constant ICON. */
	private static final ModernIcon ICON =
			UIService.getInstance().loadIcon("log", 24);
	
	/**
	 * The member window.
	 */
	private MainMatCalcWindow mWindow;

	/* (non-Javadoc)
	 * @see org.abh.lib.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "Log";
	}

	/* (non-Javadoc)
	 * @see edu.columbia.rdf.apps.matcalc.modules.Module#init(edu.columbia.rdf.apps.matcalc.MainMatCalcWindow)
	 */
	@Override
	public void init(MainMatCalcWindow window) {
		mWindow = window;

		/*
		RibbonLargeButton button = new RibbonLargeButton("Log 2",
				UIService.getInstance().loadIcon("log", 32),
				"Log 2", 
				"Log 2 transform the expression values in the matrix.");
		button.addClickListener(this);
		mWindow.getRibbon().getToolbar("Transform").getSection("Log").add(button);

		button = new RibbonLargeButton("Log 10", 
				UIService.getInstance().loadIcon("log", 32),
				"Log 10", 
				"Log 10 transform the expression values in the matrix.");
		button.addClickListener(this);
		mWindow.getRibbon().getToolbar("Transform").getSection("Log").add(button);

		button = new RibbonLargeButton("Ln",
				UIService.getInstance().loadIcon("log", 32),
				"Ln", 
				"Log e (natural log) transform the expression values in the matrix.");
		button.addClickListener(this);
		mWindow.getRibbon().getToolbar("Transform").getSection("Log").add(button);
		*/
		
		ModernPopupMenu popup = new ModernPopupMenu();

		popup.addMenuItem(new ModernTwoLineMenuItem("Log 2", 
				"Log 2 tranform.", 
				ICON));
		popup.addMenuItem(new ModernTwoLineMenuItem("Log 10",
				"Log 10 tranform.",
				ICON));
		popup.addMenuItem(new ModernTwoLineMenuItem("Ln",
				"Natural log tranform.",
				ICON));

		//popup.addMenuItem(new ModernMenuSeparator());
		
		popup.addMenuItem(new ModernMenuHelpItem("Help with log transforming a matrix...",
				"matcalc.modules.math.log.url").setTextOffset(48));

		// The default behaviour is to do a log2 transform.
		RibbonLargeOptionalDropDownButton button = 
				new RibbonLargeOptionalDropDownButton("Log", ICON, popup);
		button.setToolTip("Log", "Log transform a matrix.");

		mWindow.getRibbon().getToolbar("Transform").getSection("Transform").add(button);

		button.addClickListener(this);
	}

	/* (non-Javadoc)
	 * @see org.abh.lib.ui.modern.event.ModernClickListener#clicked(org.abh.lib.ui.modern.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		if (e.getMessage().equals("Log") || e.getMessage().equals("Log 2")) {
			mWindow.addToHistory("log2", "log2", log2(mWindow, mWindow.getCurrentMatrix(), 1)); //new Log2MatrixTransform(this, getCurrentMatrix(), 1));
		} else if (e.getMessage().equals("Log 10")) {
			mWindow.addToHistory("log10", "log10", log10(mWindow, mWindow.getCurrentMatrix(), 1)); //new Log10MatrixTransform(this, getCurrentMatrix(), 1));
		} else if (e.getMessage().equals("Ln")) {
			mWindow.addToHistory("ln", "ln", ln(mWindow, mWindow.getCurrentMatrix(), 1)); //new NaturalLogMatrixTransform(this, getCurrentMatrix(), 1));
		}
	}
	
	/**
	 * Log2.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param min the min
	 * @return the annotation matrix
	 */
	public static AnnotationMatrix log2(ModernWindow parent, 
			AnnotationMatrix matrix, 
			double min) {
		LogDialog dialog = new LogDialog(parent, min, 2, false);
		
		dialog.setVisible(true);
		
		return log(parent, matrix, dialog);
	}
	
	/**
	 * Log10.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param min the min
	 * @return the annotation matrix
	 */
	public static AnnotationMatrix log10(ModernWindow parent, 
			AnnotationMatrix matrix, 
			double min) {
		LogDialog dialog = new LogDialog(parent, min, 10, false);
		
		dialog.setVisible(true);
		
		return log(parent, matrix, dialog);
	}
	
	/**
	 * Ln.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param min the min
	 * @return the annotation matrix
	 */
	public static AnnotationMatrix ln(ModernWindow parent, 
			AnnotationMatrix matrix, 
			double min) {
		LogDialog dialog = new LogDialog(parent, min, 2, true);
		
		dialog.setVisible(true);
		
		return log(parent, matrix, dialog);
	}
	
	/**
	 * Log.
	 *
	 * @param parent the parent
	 * @param matrix the matrix
	 * @param dialog the dialog
	 * @return the annotation matrix
	 */
	public static AnnotationMatrix log(ModernWindow parent, 
			AnnotationMatrix matrix, 
			LogDialog dialog) {

		if (dialog.getStatus() == ModernDialogStatus.OK) {
			AnnotationMatrix m;
			
			if (dialog.getAdd()) {
				m = MatrixOperations.add(matrix, dialog.getAddAmount());
			} else if (dialog.getMin()) {
				m = MatrixOperations.min(matrix, dialog.getMinAmount());
			} else {
				m = matrix;
			}
			
			if (dialog.getNatural()) {
				return MatrixOperations.ln(m);
			} else {
				AnnotationMatrix ret = MatrixOperations.log(m, dialog.getBase());

				return ret;
			}
		} else {
			return null;
		}
	}
}
