/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import java.util.ArrayList;
import java.util.List;

import org.abh.common.PluginService;
import org.matcalc.toolbox.Module;
import org.matcalc.toolbox.core.ColumnAnnotationModule;
import org.matcalc.toolbox.core.ExtractDataModule;
import org.matcalc.toolbox.core.GroupModule;
import org.matcalc.toolbox.core.SplitModule;
import org.matcalc.toolbox.core.SummaryModule;
import org.matcalc.toolbox.core.ViewModule;
import org.matcalc.toolbox.core.ZoomModule;
import org.matcalc.toolbox.core.collapse.CollapseModule;
import org.matcalc.toolbox.core.duplicate.DuplicateModule;
import org.matcalc.toolbox.core.filter.FilterModule;
import org.matcalc.toolbox.core.filter.column.ColumnFilterModule;
import org.matcalc.toolbox.core.filter.row.RowFilterModule;
import org.matcalc.toolbox.core.io.CsvIOModule;
import org.matcalc.toolbox.core.io.ExcelIOModule;
import org.matcalc.toolbox.core.io.MatrixIOModule;
import org.matcalc.toolbox.core.io.TsvIOModule;
import org.matcalc.toolbox.core.io.TxtIOModule;
import org.matcalc.toolbox.core.io.XlsIOModule;
import org.matcalc.toolbox.core.io.XlsxIOModule;
import org.matcalc.toolbox.core.match.MatchModule;
import org.matcalc.toolbox.core.roworder.RowOrderModule;
import org.matcalc.toolbox.core.search.SearchColumnModule;
import org.matcalc.toolbox.core.shift.ShiftModule;
import org.matcalc.toolbox.core.sort.SortColumnsByRowModule;
import org.matcalc.toolbox.core.sort.SortModule;
import org.matcalc.toolbox.core.venn.VennModule;
import org.matcalc.toolbox.math.LogModule;
import org.matcalc.toolbox.math.NormalizeModule;
import org.matcalc.toolbox.math.PowerModule;
import org.matcalc.toolbox.math.QuantileNormalizeModule;
import org.matcalc.toolbox.math.TransposeModule;
import org.matcalc.toolbox.math.ZScoreModule;
import org.matcalc.toolbox.plot.barchart.BarChartModule;
import org.matcalc.toolbox.plot.barchart.HistogramModule;
import org.matcalc.toolbox.plot.barchart.StackedBarChartModule;
import org.matcalc.toolbox.plot.boxwhisker.BoxWhiskerPlotModule;
import org.matcalc.toolbox.plot.boxwhisker.BoxWhiskerScatterPlotModule;
import org.matcalc.toolbox.plot.heatmap.cluster.legacy.LegacyClusterModule;
import org.matcalc.toolbox.plot.heatmap.legacy.LegacyHeatMapModule;
import org.matcalc.toolbox.plot.scatter.ScatterModule;
import org.matcalc.toolbox.plot.scatter.SmoothedLineGraphModule;
import org.matcalc.toolbox.plot.volcano.VolcanoPlotModule;
import org.matcalc.toolbox.stats.ttest.legacy.TTestModule;

// TODO: Auto-generated Javadoc
/**
 * The Class ModuleLoader.
 */
public class ModuleLoader {
	
	/** The m modules list. */
	private List<Class<? extends Module>> mModulesList = 
			new ArrayList<Class<? extends Module>>();
	
	/**
	 * Instantiates a new module loader.
	 */
	public ModuleLoader() {
		addModule(MatrixIOModule.class);
		addModule(ExcelIOModule.class);
		addModule(XlsxIOModule.class);
		addModule(XlsIOModule.class);
		addModule(TxtIOModule.class);
		addModule(TsvIOModule.class);
		addModule(CsvIOModule.class);
		addModule(LegacyHeatMapModule.class);
		addModule(LegacyClusterModule.class);
		addModule(SmoothedLineGraphModule.class);
		//addModule(ScatterLineModule.class);
		addModule(ScatterModule.class);
		addModule(BarChartModule.class);
		//addModule(BarChartHModule.class);
		addModule(StackedBarChartModule.class);
		addModule(HistogramModule.class);
		//addModule(PieChartModule.class);
		addModule(BoxWhiskerPlotModule.class);
		addModule(BoxWhiskerScatterPlotModule.class);
		addModule(VolcanoPlotModule.class);
		addModule(ShiftModule.class);
		
		addModule(TransposeModule.class);
		addModule(LogModule.class);
		addModule(PowerModule.class);
		addModule(ZScoreModule.class);
		addModule(NormalizeModule.class);
		addModule(QuantileNormalizeModule.class);
		
		addModule(SortModule.class);
		addModule(SortColumnsByRowModule.class);
		addModule(FilterModule.class);
		addModule(RowFilterModule.class);
		addModule(ColumnFilterModule.class);
		addModule(SearchColumnModule.class);
		addModule(MatchModule.class);
		
		
		addModule(SummaryModule.class);
		addModule(CollapseModule.class);
		addModule(DuplicateModule.class);
		addModule(SplitModule.class);
		addModule(ExtractDataModule.class);
		addModule(GroupModule.class);
		addModule(ColumnAnnotationModule.class);
		
		addModule(RowOrderModule.class);
		addModule(ColumnFilterModule.class);
		addModule(TTestModule.class);
		addModule(VennModule.class);
		addModule(ZoomModule.class);
		addModule(ViewModule.class);
		
	}
	
	/**
	 * Adds the module.
	 *
	 * @param c the c
	 * @return the module loader
	 */
	public ModuleLoader addModule(Class<? extends Module> c) {
		//PluginService.getInstance().addPlugin(c);
		
		mModulesList.add(c);
		
		return this;
	}
	
	/**
	 * Load modules.
	 */
	public void loadModules() {
		for (Class<?> c : mModulesList) {
			PluginService.getInstance().addPlugin(c);
		}
	}
}
