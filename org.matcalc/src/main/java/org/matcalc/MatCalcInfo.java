/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import org.abh.common.AppVersion;
import org.abh.common.ui.UIService;
import org.abh.common.ui.help.GuiAppInfo;
import org.matcalc.icons.MatCalcIcon;

// TODO: Auto-generated Javadoc
/**
 * The class MatCalcInfo.
 */
public class MatCalcInfo extends GuiAppInfo {

	/**
	 * Instantiates a new mat calc info.
	 */
	public MatCalcInfo() {
		super("MatCalc",
				new AppVersion(11),
				"Copyright (C) 2014-${year} Antony Holmes",
				UIService.getInstance().loadIcon(MatCalcIcon.class, 32),
				UIService.getInstance().loadIcon(MatCalcIcon.class, 128),
				"Matrix calculations and plotting.");
	}

}
