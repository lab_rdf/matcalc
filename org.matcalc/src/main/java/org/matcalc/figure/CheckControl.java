/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import org.abh.common.Properties;
import org.abh.common.ui.UI;
import org.abh.common.ui.button.CheckBox;
import org.abh.common.ui.button.ModernCheckSwitch;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.window.ModernWindow;

// TODO: Auto-generated Javadoc
/**
 * The class IntensityControl.
 */
public class CheckControl extends VBox {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/** The m check. */
	private CheckBox mCheck;
	
	/** The m properties. */
	private Properties mProperties;

	/** The m setting. */
	private String mSetting;
	
	/**
	 * The member spinner.
	 *
	 * @param parent the parent
	 * @param name the name
	 * @param properties the properties
	 * @param setting the setting
	 */
	//private ModernCompactSpinner mSpinner = 
	//		new ModernCompactSpinner(1, 10, 3);

	public CheckControl(ModernWindow parent,
			String name,
			Properties properties, 
			String setting) {
		mSetting = setting;
		mProperties = properties;
		
		mCheck = new ModernCheckSwitch(name);
		
		mCheck.setSelected(properties.getAsBool(setting));

		add(UI.createVGap(5));
		add(mCheck);
		add(UI.createVGap(5));
		
		mCheck.addClickListener(new ModernClickListener() {
			@Override
			public void clicked(ModernClickEvent e) {
				mProperties.setProperty(mSetting, mCheck.isSelected());
			}});
	}
}
