/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import org.abh.common.ui.UIService;
import org.abh.common.ui.graphics.icons.ModernIcon;
import org.abh.common.ui.table.ModernTableCheckboxCellRenderer;
import org.abh.common.ui.theme.ThemeService;


// TODO: Auto-generated Javadoc
/**
 * Displays an icon in a table cell.
 * 
 * @author Antony Holmes Holmes
 *
 */
public class ModernTableLayerCellRenderer extends ModernTableCheckboxCellRenderer {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The constant VISIBLE_ICON.
	 */
	private final static ModernIcon VISIBLE_ICON = 
			UIService.getInstance().loadIcon("layer_visible", UIService.ICON_SIZE_16);
	
	/**
	 * The constant INVISIBLE_ICON.
	 */
	private final static ModernIcon INVISIBLE_ICON = 
			UIService.getInstance().loadIcon("blank", UIService.ICON_SIZE_16);

	/**
	 * Instantiates a new modern table layer cell renderer.
	 */
	public ModernTableLayerCellRenderer() {
		setCanHighlight(false);
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.table.ModernTableCheckboxCellRenderer#drawForegroundAA(java.awt.Graphics2D)
	 */
	public void drawForegroundAAText(Graphics2D g2) {

		int x = (this.getWidth() - UIService.ICON_SIZE_16) / 2;
		int y = (this.getHeight() - UIService.ICON_SIZE_16) / 2;
		
		if (selected) {
			VISIBLE_ICON.drawIcon(g2, x, y, UIService.ICON_SIZE_16);
		} else {
			INVISIBLE_ICON.drawIcon(g2, x, y, UIService.ICON_SIZE_16);
		}
		
		x = (this.getWidth() - UIService.ICON_SIZE_20) / 2;
		y = (this.getHeight() - UIService.ICON_SIZE_20) / 2;
		
		drawRect(g2, 
				ThemeService.getInstance().colors().getColorHighlight(2), 
				new Rectangle(x, y, UIService.ICON_SIZE_20, UIService.ICON_SIZE_20));
	}
}