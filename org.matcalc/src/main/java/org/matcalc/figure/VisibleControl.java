/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.ui.button.CheckBox;
import org.abh.common.ui.button.ModernCheckBox;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.window.ModernWindow;
import org.graphplot.figure.properties.VisibleProperties;

// TODO: Auto-generated Javadoc
/**
 * The class AxisVisibleControl.
 */
public class VisibleControl extends HBox implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The member check box.
	 */
	private CheckBox mCheckBox;

	/**
	 * The member axis.
	 */
	private VisibleProperties mVp;
	
	
	/**
	 * Instantiates a new axis visible control.
	 *
	 * @param parent the parent
	 * @param vp the vp
	 */
	public VisibleControl(ModernWindow parent, VisibleProperties vp) {
		mCheckBox = new ModernCheckBox("Show");
		
		add(mCheckBox);
		//setBorder(ModernWidget.BORDER);

		mCheckBox.addClickListener(this);
		
		changeProperties(vp);
		
		mVp.addChangeListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent e) {
				mCheckBox.setSelected(mVp.getVisible());
			}});
	}
	
	/**
	 * Change properties.
	 *
	 * @param vp the vp
	 */
	public void changeProperties(VisibleProperties vp) {
		mVp = vp;
		
		mCheckBox.setSelected(vp.getVisible());
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return mCheckBox.isSelected();
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		mVp.setVisible(mCheckBox.isSelected());
	}

}
