/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.HExBox;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernClipboardTextField;
import org.abh.common.ui.text.ModernTextBorderPanel;
import org.abh.common.ui.text.ModernTextField;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;
import org.graphplot.figure.properties.TitleProperties;

// TODO: Auto-generated Javadoc
/**
 * The class PlotTitleControl.
 */
public class PlotTitleControl extends HExBox implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The member title.
	 */
	private TitleProperties mTitle;
	
	/**
	 * The member text field.
	 */
	private ModernTextField mTextField = new ModernClipboardTextField();
	
	/**
	 * The class KeyEvents.
	 */
	private class KeyEvents implements KeyListener {

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				update();
			}
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Instantiates a new plot title control.
	 *
	 * @param parent the parent
	 * @param title the title
	 */
	public PlotTitleControl(ModernWindow parent, TitleProperties title) {	
		mTitle = title;
		
		mTextField.setText(title.getText());
		
		add(new ModernAutoSizeLabel("Title", ModernWidget.TINY_SIZE));
		add(new ModernTextBorderPanel(mTextField, ModernWidget.EXTRA_LARGE_SIZE));

		mTextField.addKeyListener(new KeyEvents());
		
		title.addChangeListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent e) {
				mTextField.setText(mTitle.getText());
			}});
	}
	
	/**
	 * Update.
	 */
	private void update() {
		mTitle.setText(mTextField.getText());
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		update();
	}

}
