/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import java.awt.Color;

import org.abh.common.Properties;
import org.abh.common.ui.UI;
import org.abh.common.ui.button.ModernCheckSwitch;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.color.ColorSwatchButton;
import org.abh.common.ui.panel.HExpandBox;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.widget.ModernTwoStateWidget;
import org.abh.common.ui.window.ModernWindow;
import org.graphplot.figure.heatmap.legacy.GroupProperties;

// TODO: Auto-generated Javadoc
/**
 * The class GroupsPlotControl.
 */
public class GroupsPlotControl extends VBox {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The check box.
	 */
	//private CheckBox mCheckShow = new ModernCheckBox("Labels");
	
	/**
	 * The check color.
	 */
	private ModernTwoStateWidget mCheckColor = 
			new ModernCheckSwitch("Show", true);
	
	/**
	 * The check grid.
	 */
	private ModernTwoStateWidget mCheckGrid = 
			new ModernCheckSwitch("Grid", false);
	
	/** The m check legend. */
	private ModernTwoStateWidget mCheckLegend = 
			new ModernCheckSwitch("Legend", true);
	
	/**
	 * The check border.
	 */
	//private CheckBox mGheckBorder = new ModernCheckBox("Border", true);
	
	/**
	 * The label color button.
	 */
	//private ColorSwatchButton mLabelColorButton;

	/**
	 * The border color button.
	 */
	private ColorSwatchButton mBorderColorButton;

	/**
	 * The grid color button.
	 */
	private ColorSwatchButton mGridColorButton;
	
	/**
	 * Instantiates a new groups plot control.
	 *
	 * @param parent the parent
	 * @param color the color
	 * @param properties the properties
	 */
	public GroupsPlotControl(ModernWindow parent, 
			Color color,
			Properties properties) {
		
		/*
		if (labelOption) {
			mLabelColorButton = new ColorSwatchButton(parent, color);

			add(new HExpandBox(mCheckShow, mLabelColorButton));

			add(ModernPanel.createVGap());
		}
		*/
		
		add(mCheckColor);
		
		add(UI.createVGap(5));
		
		//Box box = VBox.create();
		
		mGridColorButton = new ColorSwatchButton(parent, color);
		add(new HExpandBox(mCheckGrid, mGridColorButton));
		
		add(UI.createVGap(5));
		add(mCheckLegend);
		
		mCheckLegend.setSelected(properties.getAsBool("plot.show-legend"));
		
		//mBorderColorButton = new ColorSwatchButton(parent, color);
		//box.add(new HExpandBox(mGheckBorder, mBorderColorButton));
		//box.setBorder(BorderService.getInstance().createLeftBorder(10));
		
		//add(box);
	}
	
	/**
	 * Adds the click listener.
	 *
	 * @param l the l
	 */
	public void addClickListener(ModernClickListener l) {
		//mCheckShow.addClickListener(l);
		//mLabelColorButton.addClickListener(l);
		mCheckColor.addClickListener(l);
		//mGheckBorder.addClickListener(l);
		//mBorderColorButton.addClickListener(l);
		mCheckGrid.addClickListener(l);
		mGridColorButton.addClickListener(l);
		mCheckLegend.addClickListener(l);
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	//public boolean isSelected() {
	//	return mCheckShow.isSelected();
	//}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	//public Color getColor() {
	//	return mLabelColorButton.getSelectedColor();
	//}

	/**
	 * Gets the show colors.
	 *
	 * @return the show colors
	 */
	public boolean getShowColors() {
		return mCheckColor.isSelected();
	}
	
	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public GroupProperties getProperties() {
		GroupProperties properties = new GroupProperties();
		//properties.show = isSelected();
		//properties.color = getColor();
		properties.showColors = getShowColors();
		//properties.showBorder = getShowBorder();
		//properties.borderColor = getBorderColor();
		properties.showGrid = getShowGrid();
		properties.gridColor = getGridColor();

		return properties;
	}

	/**
	 * Gets the show grid.
	 *
	 * @return the show grid
	 */
	private boolean getShowGrid() {
		return mCheckGrid.isSelected();
	}
	
	/**
	 * Gets the grid color.
	 *
	 * @return the grid color
	 */
	public Color getGridColor() {
		return mGridColorButton.getSelectedColor();
	}

	/**
	 * Gets the show border.
	 *
	 * @return the show border
	 */
	//private boolean getShowBorder() {
	//	return mGheckBorder.isSelected();
	//}

	/**
	 * Gets the border color.
	 *
	 * @return the border color
	 */
	public Color getBorderColor() {
		return mBorderColorButton.getSelectedColor();
	}

	/**
	 * Gets the show legend.
	 *
	 * @return the show legend
	 */
	public Object getShowLegend() {
		return mCheckLegend.isSelected();
	}
}
