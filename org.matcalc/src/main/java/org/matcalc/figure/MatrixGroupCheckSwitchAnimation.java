package org.matcalc.figure;

import java.awt.Color;
import java.awt.Graphics2D;

import org.abh.common.ui.button.CheckSwitchAnimation;
import org.abh.common.ui.widget.ModernWidget;

public class MatrixGroupCheckSwitchAnimation extends CheckSwitchAnimation {

	private Color mColor;

	public MatrixGroupCheckSwitchAnimation(ModernWidget widget, Color color) {
		super(widget);
		
		mColor = color;
	}
	
	@Override
	public void setSelectedColor(Graphics2D g2) {
		g2.setColor(mColor);
	}

}
