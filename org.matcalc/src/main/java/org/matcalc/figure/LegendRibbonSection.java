/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc.figure;

import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.ribbon.Ribbon;
import org.abh.common.ui.ribbon.RibbonLargeCheckButton;
import org.abh.common.ui.ribbon.RibbonSection;
import org.graphplot.figure.properties.LegendProperties;



// TODO: Auto-generated Javadoc
/**
 * Allows user to select a color map.
 *
 * @author Antony Holmes Holmes
 *
 */
public class LegendRibbonSection extends RibbonSection {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The member legend.
	 */
	private LegendProperties mLegend;

	/**
	 * The show legend button.
	 */
	private RibbonLargeCheckButton mShowButton = 
			new RibbonLargeCheckButton("Show");
	
	/** The m show border button. */
	private RibbonLargeCheckButton mShowBorderButton = 
			new RibbonLargeCheckButton("Border");
	
	/** The m show background button. */
	private RibbonLargeCheckButton mShowBackgroundButton = 
			new RibbonLargeCheckButton("Background");

	/**
	 * The legend position picker.
	 */
	private LegendPositionPicker legendPositionPicker;
	
	/**
	 * Instantiates a new legend ribbon section.
	 *
	 * @param ribbon the ribbon
	 * @param legend the legend
	 */
	public LegendRibbonSection(Ribbon ribbon, LegendProperties legend) {
		super(ribbon, "Legend");
		
		mLegend = legend;
		
		add(mShowButton, mShowBorderButton, mShowBackgroundButton);
		add(ModernPanel.createHGap());
		addSeparator();
		//add(ModernPanel.createHGap());
		legendPositionPicker = new LegendPositionPicker(legend);
		add(legendPositionPicker);
		
		
		mShowButton.setSelected(legend.getVisible());
		mShowBorderButton.setSelected(legend.getStyle().getLineStyle().getVisible());
		mShowBackgroundButton.setSelected(legend.getStyle().getFillStyle().getVisible());
		
		mShowButton.addClickListener(new ModernClickListener() {
			@Override
			public void clicked(ModernClickEvent e) {
				mLegend.setVisible(mShowButton.isSelected());
			}});
		
		mShowBorderButton.addClickListener(new ModernClickListener() {
			@Override
			public void clicked(ModernClickEvent e) {
				mLegend.getStyle().getLineStyle().setVisible(mShowBorderButton.isSelected());
			}});
		
		mShowBackgroundButton.addClickListener(new ModernClickListener() {
			@Override
			public void clicked(ModernClickEvent e) {
				mLegend.getStyle().getFillStyle().setVisible(mShowBackgroundButton.isSelected());
			}});
		
		mLegend.addChangeListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent e) {
				mShowButton.setSelected(mLegend.getVisible());
				mShowBorderButton.setSelected(mLegend.getStyle().getLineStyle().getVisible());
				mShowBackgroundButton.setSelected(mLegend.getStyle().getFillStyle().getVisible());
			}});
	}
}
