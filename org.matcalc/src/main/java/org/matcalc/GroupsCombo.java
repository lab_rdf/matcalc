/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import org.abh.common.math.matrix.MatrixGroup;
import org.abh.common.ui.UI;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.widget.ModernWidget;
import org.graphplot.figure.series.XYSeriesGroup;

// TODO: Auto-generated Javadoc
/**
 * The Class GroupsCombo.
 */
public class GroupsCombo extends ModernComboBox {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new groups combo.
	 *
	 * @param groups the groups
	 */
	public GroupsCombo(XYSeriesGroup groups) {
		for (MatrixGroup group : groups) {
			addMenuItem(new GroupMenuItem(group));
		}
		
		UI.setSize(this, ModernWidget.EXTRA_LARGE_SIZE);
	}
}
