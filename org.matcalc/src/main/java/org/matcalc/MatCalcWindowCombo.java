/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import java.awt.Dimension;

import org.abh.common.ui.UI;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;
import org.abh.common.ui.window.WindowService;

// TODO: Auto-generated Javadoc
/**
 * The class MatCalcWindowCombo.
 */
public class MatCalcWindowCombo extends ModernComboBox {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The constant SIZE.
	 */
	private static final Dimension SIZE = 
			new Dimension(400, ModernWidget.WIDGET_HEIGHT);

	/**
	 * Instantiates a new mat calc window combo.
	 */
	public MatCalcWindowCombo() {
		for (ModernWindow window : WindowService.getInstance()) {
			if (!(window instanceof MainMatCalcWindow)) {
				continue;
			}
			
			addMenuItem(window.getSubTitle());
		}
		
		UI.setSize(this, SIZE);
	}
}
