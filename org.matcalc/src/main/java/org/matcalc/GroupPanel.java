/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import javax.swing.Box;

import org.abh.common.ui.button.CheckBox;
import org.abh.common.ui.button.ModernCheckBox;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernLabel;
import org.graphplot.figure.series.XYSeries;
import org.graphplot.figure.series.XYSeriesGroup;

// TODO: Auto-generated Javadoc
/**
 * For choosing an FDR method.
 *
 * @author Antony Holmes Holmes
 */
public class GroupPanel extends HBox {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The group1 combo.
	 */
	private ModernComboBox group1Combo;
	
	/**
	 * The group2 combo.
	 */
	private ModernComboBox group2Combo;
	
	/**
	 * The check equal variance.
	 */
	private CheckBox checkEqualVariance = 
			new ModernCheckBox("Equal variance");
	
	/**
	 * The member groups.
	 */
	private XYSeriesGroup mGroups;

	/**
	 * Instantiates a new group panel.
	 *
	 * @param groups the groups
	 */
	public GroupPanel(XYSeriesGroup groups) {
		mGroups = groups;
		
		group1Combo = new GroupsCombo(groups);
		group2Combo = new GroupsCombo(groups);
		
		group1Combo.setSelectedIndex(0);
		group2Combo.setSelectedIndex(1);
		
		ModernLabel label = new ModernAutoSizeLabel("Groups");
		label.setAlignmentY(TOP_ALIGNMENT);
		
		
		add(label);
		add(Box.createHorizontalGlue());
		
		Box box = VBox.create();
		box.setAlignmentY(TOP_ALIGNMENT);
		box.add(group1Combo);
		box.add(ModernPanel.createVGap());
		box.add(new ModernAutoSizeLabel("vs"));
		box.add(ModernPanel.createVGap());
		box.add(group2Combo);
		
		add(box);
		
		//setAlignmentY(TOP_ALIGNMENT);
	}
	
	/**
	 * Gets the equal variance.
	 *
	 * @return the equal variance
	 */
	public boolean getEqualVariance() {
		return checkEqualVariance.isSelected();
	}
	
	/**
	 * Gets the group1.
	 *
	 * @return the group1
	 */
	public XYSeries getGroup1() {
		if (mGroups == null || mGroups.getCount() == 0) {
			return null;
		}
		
		return mGroups.get(group1Combo.getSelectedIndex());
	}
	
	/**
	 * Gets the group2.
	 *
	 * @return the group2
	 */
	public XYSeries getGroup2() {
		if (mGroups == null || mGroups.getCount() == 0) {
			return null;
		}
		
		return mGroups.get(group2Combo.getSelectedIndex());
	}
}
