/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matcalc;

import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.UI;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.widget.ModernWidget;

// TODO: Auto-generated Javadoc
/**
 * For choosing an FDR method.
 *
 * @author Antony Holmes Holmes
 */
public class MatrixRowAnnotationCombo extends ModernComboBox {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new matrix row annotation combo.
	 *
	 * @param matrix the matrix
	 */
	public MatrixRowAnnotationCombo(AnnotationMatrix matrix) {
		for (String name : matrix.getRowAnnotationNames()) {
			addMenuItem(name);
		}
		
		UI.setSize(this, ModernWidget.EXTRA_LARGE_SIZE);
	}
}
