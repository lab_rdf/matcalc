# README #

## Download And Installation

* Download the latest version from [Downloads](https://bitbucket.org/lab_rdf/matcalc/downloads/).
* Unzip MatCalc into a directory of your choice.
* MatCalc does not require installation and therefore does not require administrator privileges to run.

## Running

MatCalc requires Java 1.7 or above and the Java installation must have been added to the system path so it can be discovered.

* **Windows** - Double click matcalc.bat.
* **macOS** - Double click matcalc.command.
* **Linux** - Run matcalc.sh.

Please read the [Wiki](https://bitbucket.org/lab_rdf/matcalc/wiki/Home) for more detailed usage instructions.